#ifndef USEROBJECT_H
#define USEROBJECT_H

#include <boost/multiprecision/cpp_int.hpp>
#include <commons/trim.h>
#include <commons/Logger.h>

#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <atomic>
#include <algorithm>
#include <memory>
#include <sys/time.h>

using namespace boost::multiprecision;

class BaseHandler;

class UserObject
{
public:

	static std::map<std::string, UserObject*> userHandler;	// ключ это ссылка на юзернейм

	enum EventCode { ANSWER = 1, NOT_ANSWER, TOUT, ERROR_EVENT };

	static void UserThread(UserObject* uo);

	UserObject(std::string& uname, uint32_t uId);

	void sendXmpp(std::string text);

	void sendConsole(std::string text);

	/// @return enum eventCode
	EventCode waitEvent(std::string& cmd, std::string& pars);

	std::string& userName();
	uint32_t userId();

	std::unique_ptr<std::thread> thr_;
	std::condition_variable cv_;
	std::mutex mt_;
	std::queue<std::string> events_;
	uint256_t lastTime;

private:
	std::string uname_;
	uint32_t uId_;
	bool stopThread = false;

	BaseHandler* handler = nullptr;
};

#endif

