-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.8.1
-- PostgreSQL version: 9.4
-- Project Site: pgmodeler.com.br
-- Model Author: ---


-- Database creation must be done outside an multicommand file.
-- These commands were put in this file only for convenience.
-- -- object: deusex | type: DATABASE --
-- -- DROP DATABASE IF EXISTS deusex;
-- CREATE DATABASE deusex
-- ;
-- -- ddl-end --
-- 

-- object: public.users | type: TABLE --
-- DROP TABLE IF EXISTS public.users CASCADE;
CREATE TABLE public.users(
	id serial NOT NULL,
	username varchar(64),
	account varchar(128) NOT NULL,
	file varchar(128) NOT NULL,
	"position" bigint,
	suspended boolean NOT NULL,
	CONSTRAINT user_id PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE public.users OWNER TO postgres;
-- ddl-end --

-- object: public.nodes | type: TABLE --
-- DROP TABLE IF EXISTS public.nodes CASCADE;
CREATE TABLE public.nodes(
	id serial NOT NULL,
	cluster_id integer NOT NULL,
	system_id integer NOT NULL,
	name varchar(64) NOT NULL,
	type varchar(64) NOT NULL,
	defence_code varchar(128),
	attack_code varchar(128),
	encrypted boolean NOT NULL,
	"desc" varchar(1024),
	info varchar(1024) NOT NULL,
	CONSTRAINT node_id PRIMARY KEY (id)

);
-- ddl-end --
COMMENT ON TABLE public.nodes IS 'Таблица всех узлов всех сетей';
-- ddl-end --
COMMENT ON COLUMN public.nodes."desc" IS 'Описание локации';
-- ddl-end --
COMMENT ON COLUMN public.nodes.info IS 'Инфа найденная в локации';
-- ddl-end --
ALTER TABLE public.nodes OWNER TO postgres;
-- ddl-end --

-- object: public.edges | type: TABLE --
-- DROP TABLE IF EXISTS public.edges CASCADE;
CREATE TABLE public.edges(
	id serial NOT NULL,
	"from" bigint NOT NULL,
	"to" bigint NOT NULL,
	CONSTRAINT edge_id PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE public.edges OWNER TO postgres;
-- ddl-end --

-- object: public.clusters | type: TABLE --
-- DROP TABLE IF EXISTS public.clusters CASCADE;
CREATE TABLE public.clusters(
	id serial NOT NULL,
	name varchar(64) NOT NULL,
	"desc" varchar(1024) NOT NULL,
	CONSTRAINT cluster_id PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE public.clusters OWNER TO postgres;
-- ddl-end --

-- object: public.systems | type: TABLE --
-- DROP TABLE IF EXISTS public.systems CASCADE;
CREATE TABLE public.systems(
	id serial NOT NULL,
	name varchar(64) NOT NULL,
	"desc" varchar(1024) NOT NULL,
	CONSTRAINT system_id PRIMARY KEY (id)

)WITH ( OIDS = TRUE );
-- ddl-end --
ALTER TABLE public.systems OWNER TO postgres;
-- ddl-end --

-- object: public.ways | type: TABLE --
-- DROP TABLE IF EXISTS public.ways CASCADE;
CREATE TABLE public.ways(
	id serial NOT NULL,
	user_id smallint NOT NULL,
	node_id bigint NOT NULL,
	"time" timestamp NOT NULL,
	CONSTRAINT way_id PRIMARY KEY (id)

);
-- ddl-end --
COMMENT ON TABLE public.ways IS 'Здесь логируется путь юзера по ребрам';
-- ddl-end --
COMMENT ON COLUMN public.ways."time" IS 'Время удачного вскрытия узла';
-- ddl-end --
ALTER TABLE public.ways OWNER TO postgres;
-- ddl-end --

-- object: public.defences | type: TABLE --
-- DROP TABLE IF EXISTS public.defences CASCADE;
CREATE TABLE public.defences(
	defence_code varchar(128) NOT NULL,
	init_code varchar(128) NOT NULL,
	types varchar(1024) NOT NULL,
	effect_m varchar(64) NOT NULL,
	effect_o varchar(64) NOT NULL,
	compromated boolean NOT NULL,
	"desc" varchar(1024),
	user_id smallint NOT NULL,
	"time" timestamp NOT NULL,
	CONSTRAINT defence_id PRIMARY KEY (defence_code)

);
-- ddl-end --
COMMENT ON COLUMN public.defences.init_code IS 'Код активации защиты';
-- ddl-end --
COMMENT ON COLUMN public.defences.compromated IS 'Флаг выставляется, когда защита скомпрометирована в нашей системе безопасности';
-- ddl-end --
ALTER TABLE public.defences OWNER TO postgres;
-- ddl-end --

-- object: public.attacs | type: TABLE --
-- DROP TABLE IF EXISTS public.attacs CASCADE;
CREATE TABLE public.attacs(
	attack_code varchar(128) NOT NULL,
	types varchar(1024) NOT NULL,
	effect varchar(64) NOT NULL,
	compromated boolean NOT NULL,
	"desc" varchar(1024) NOT NULL,
	user_id smallint NOT NULL,
	"time" timestamp,
	CONSTRAINT attack_id PRIMARY KEY (attack_code)

);
-- ddl-end --
COMMENT ON COLUMN public.attacs.compromated IS 'Флаг выставляется, когда атака становится всем известной и теряет эффективность';
-- ddl-end --
COMMENT ON COLUMN public.attacs.user_id IS 'Кто добавил атаку';
-- ddl-end --
ALTER TABLE public.attacs OWNER TO postgres;
-- ddl-end --

-- object: rel_position_to_node | type: CONSTRAINT --
-- ALTER TABLE public.users DROP CONSTRAINT IF EXISTS rel_position_to_node CASCADE;
ALTER TABLE public.users ADD CONSTRAINT rel_position_to_node FOREIGN KEY ("position")
REFERENCES public.nodes (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: rel_system_to_node | type: CONSTRAINT --
-- ALTER TABLE public.nodes DROP CONSTRAINT IF EXISTS rel_system_to_node CASCADE;
ALTER TABLE public.nodes ADD CONSTRAINT rel_system_to_node FOREIGN KEY (system_id)
REFERENCES public.systems (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: rel_defence | type: CONSTRAINT --
-- ALTER TABLE public.nodes DROP CONSTRAINT IF EXISTS rel_defence CASCADE;
ALTER TABLE public.nodes ADD CONSTRAINT rel_defence FOREIGN KEY (defence_code)
REFERENCES public.defences (defence_code) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: rel_attacks | type: CONSTRAINT --
-- ALTER TABLE public.nodes DROP CONSTRAINT IF EXISTS rel_attacks CASCADE;
ALTER TABLE public.nodes ADD CONSTRAINT rel_attacks FOREIGN KEY (attack_code)
REFERENCES public.attacs (attack_code) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: rel_node_to_edge | type: CONSTRAINT --
-- ALTER TABLE public.edges DROP CONSTRAINT IF EXISTS rel_node_to_edge CASCADE;
ALTER TABLE public.edges ADD CONSTRAINT rel_node_to_edge FOREIGN KEY ("to")
REFERENCES public.nodes (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: rel_node_from_edge | type: CONSTRAINT --
-- ALTER TABLE public.edges DROP CONSTRAINT IF EXISTS rel_node_from_edge CASCADE;
ALTER TABLE public.edges ADD CONSTRAINT rel_node_from_edge FOREIGN KEY ("from")
REFERENCES public.nodes (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: rel_user | type: CONSTRAINT --
-- ALTER TABLE public.ways DROP CONSTRAINT IF EXISTS rel_user CASCADE;
ALTER TABLE public.ways ADD CONSTRAINT rel_user FOREIGN KEY (user_id)
REFERENCES public.users (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: rel_disabled_node | type: CONSTRAINT --
-- ALTER TABLE public.ways DROP CONSTRAINT IF EXISTS rel_disabled_node CASCADE;
ALTER TABLE public.ways ADD CONSTRAINT rel_disabled_node FOREIGN KEY (node_id)
REFERENCES public.nodes (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: ref_defence_adder | type: CONSTRAINT --
-- ALTER TABLE public.defences DROP CONSTRAINT IF EXISTS ref_defence_adder CASCADE;
ALTER TABLE public.defences ADD CONSTRAINT ref_defence_adder FOREIGN KEY (user_id)
REFERENCES public.users (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: ref_attack_adder | type: CONSTRAINT --
-- ALTER TABLE public.attacs DROP CONSTRAINT IF EXISTS ref_attack_adder CASCADE;
ALTER TABLE public.attacs ADD CONSTRAINT ref_attack_adder FOREIGN KEY (user_id)
REFERENCES public.users (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --


