//============================================================================
// Name        : HttpServer.h
// Author      : aav
// Created on  : 21 февр. 2016 г.
// Version     : v.0.1
// Copyright   : Non nobis, Domine, non nobis, sed nomini tuo da gloriam.
// Description : File logger based by boost library libboost_log
//============================================================================

#ifndef HTTPSERVER_H_
#define HTTPSERVER_H_

#include <mongoose/mongoose.h>

#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>

#include <iostream>
#include <vector>

#include <boost/thread/thread.hpp>
#include <boost/smart_ptr.hpp>

#include <managers/SetCommandTo.h>
#include <abstract/HttpControl.h>

/*
 * Test message for server:
 * curl -v -H "Content-Type: application/json" -X POST
 * -d '{"device":"barrier_in", "command":"down",
 * "parameters":{"state":"open", "car":"present"} }'
 * http://localhost:8000
 */

// curl -v -H "Content-Type: application/json" -X POST -d '{"device":"barrier_in", "command":"down", "parameters":{"state":"open", "car":"present"} }' http://localhost:8000

namespace httpserver
{

struct mg_serve_http_opts s_http_server_opts;

const char* kTypeNames[] =
    { "Null", "False", "True", "Object", "Array", "String", "Number" };

const std::string attrError("error"); // Опциональный атрибут, отменяет все остальные атрибуты
const std::string attrTxId("txid"); // Обязательный атрибут - номер транзакции
const std::string attrDev("device"); // Обязательный атрибут - имя абстрактного устройства
const std::string attrCmd("command"); // Обязательный атрибут - команда на устройство
const std::string attrPars("parameters"); // Опциональный атрибут - параметры команды


void cb_HttpServer(struct mg_connection* nc, int ev, void* p)
{
	using namespace rapidjson;

	struct http_message *hmsg = (struct http_message*) p;

	if (ev == MG_EV_HTTP_REQUEST)
	{
		SetTo::LocalLog("httpserver", severity_level::trace, "httpServer::cb_HttpServer: HTTP Server received new data.");

//		HOWTO: take remote address and port in mongoose
//		char* addr = inet_ntoa(nc->sa.sin.sin_addr); // client address
//		int port = nc->sa.sin.sin_port;

		nc->flags |= MG_F_SEND_AND_CLOSE;
		mg_printf(nc, "%s", "HTTP/1.1 200 OK\r\n\r\n\r\n");

		// Check body lenght
		if (hmsg->body.len == 0)
		{
			// отправить назад сообщение, что body не существует
			std::stringstream error;
			error << "ERROR! httpServer::cb_HttpServer: POST body size is NULL.";
			MT::SendError(
					MT::getNameForClient(HttpClient::s_concreteName),
					MT::getNameForClient(HttpClient::s_concreteName),
					error.str()
					);
			SetTo::LocalLog("httpserver", severity_level::error, error.str());

			return;

		}

		// Parse a JSON string into DOM.
	    std::vector< ASCII<>::Ch > json(hmsg->body.len+1, 0);
		memcpy(&json[0], hmsg->body.p, (hmsg->body.len) * sizeof(json[0]));
		json[hmsg->body.len] = 0;

		Document d;
		d.ParseInsitu(&json[0]);
		if (d.HasParseError() == true)
		{
			memcpy(&json[0], hmsg->body.p, (hmsg->body.len+1) * sizeof(json[0]));
			json[hmsg->body.len] = 0;

			// отправить назад сообщение, что json не распарсился
			std::stringstream error;
			error << "ERROR! httpServer::cb_HttpServer: JSON has wrong format: " << &json[0];
			MT::SendError(
					MT::getNameForClient(HttpClient::s_concreteName),
					MT::getNameForClient(HttpClient::s_concreteName),
					error.str()
					);

			SetTo::LocalLog("httpserver", severity_level::error, error.str());
			return;
		}


		SetTo::LocalLog("httpserver", severity_level::trace, "httpServer::cb_HttpServer: JSON was parsed correctly.");

		// Stringify the DOM
		StringBuffer buffer;
		Writer<StringBuffer> writer(buffer);
		d.Accept(writer);
		SetTo::LocalLog("httpserver", severity_level::debug, buffer.GetString());

		if (d.HasMember(attrError.c_str()) == true)
		{
			// Обработка ошибок составления документа
			Value& valError = d[attrError.c_str()];
			if (valError.IsString())
			{
				std::stringstream error;
				error << "Error has received: " << valError.GetString();

				MT::SendError(
						MT::getNameForClient(HttpClient::s_concreteName),
						MT::getNameForClient(HttpClient::s_concreteName),
						error.str()
						);
				SetTo::LocalLog("httpserver", severity_level::error, error.str());
			}
			else
			{
				std::stringstream error;
				error << "Error has received but value isn't String";

				MT::SendError(
						MT::getNameForClient(HttpClient::s_concreteName),
						MT::getNameForClient(HttpClient::s_concreteName),
						error.str()
						);
				SetTo::LocalLog("httpserver", severity_level::error, error.str());
			}

			return;
		}

		if (d.HasMember(attrDev.c_str()) == false)
		{
			// отправить сообщение, что device не найден и выйти
			std::stringstream error;
			error << "ERROR! httpServer::cb_HttpServer: JSON attribute '" << attrDev << "' not found.";
			MT::SendError(
					MT::getNameForClient(HttpClient::s_concreteName),
					MT::getNameForClient(HttpClient::s_concreteName),
					error.str()
					);
			SetTo::LocalLog("httpserver", severity_level::error, error.str());

			return;
		}

		if (d.HasMember(attrCmd.c_str()) == false)
		{
			// отправить сообщение, что command не найден и выйти
			std::stringstream error;
			error << "ERROR! httpServer::cb_HttpServer: JSON attribute '" << attrCmd << "' not found.";
			MT::SendError(
					MT::getNameForClient(HttpClient::s_concreteName),
					MT::getNameForClient(HttpClient::s_concreteName),
					error.str()
					);
			SetTo::LocalLog("httpserver", severity_level::error, error.str());

			return;
		}

		Value valParams(kObjectType);
		if (d.HasMember(attrPars.c_str()) == true)
		{
			// Если опциональные параметры существуют, то добавить из документа
			std::stringstream str;
			str << "httpServer::cb_HttpServer: JSON attribute '" << attrPars << "' has found.";
			SetTo::LocalLog("httpserver", severity_level::trace, str.str());
			valParams = d[attrPars.c_str()];
		}

		Value& valDevice = d[attrDev.c_str()];
		Value& valCommand = d[attrCmd.c_str()];

		if (valDevice.IsString() == false)
		{
			// отправить сообщение, что девайс не строка и выйти
			std::stringstream error;
			error << "ERROR! httpServer::cb_HttpServer: JSON attribute '" << attrDev << "' isn't string type";
			MT::SendError(
					MT::getNameForClient(HttpClient::s_concreteName),
					MT::getNameForClient(HttpClient::s_concreteName),
					error.str()
					);
			SetTo::LocalLog("httpserver", severity_level::error, error.str());
			return;
		}

		if (valCommand.IsString() == false)
		{
			// отправить назад сообщение, что команда не строка и выйти
			std::stringstream error;
			error << "ERROR! httpServer::cb_HttpServer: JSON attribute '" << attrCmd << "' isn't string type";
			MT::SendError(
					MT::getNameForClient(HttpClient::s_concreteName),
					MT::getNameForClient(HttpClient::s_concreteName),
					error.str()
					);
			SetTo::LocalLog("httpserver", severity_level::error, error.str());
			return;
		}

		if (valParams.IsObject() == false)
		{
			// отправить назад сообщение, что параметры не объект и выйти
			std::stringstream error;
			error << "ERROR! httpServer::cb_HttpServer: JSON attribute '" << attrPars << "' isn't object type";
			MT::SendError(
					MT::getNameForClient(HttpClient::s_concreteName),
					MT::getNameForClient(HttpClient::s_concreteName),
					error.str()
					);

			SetTo::LocalLog("httpserver", severity_level::error, error.str());
			return;
		}

		std::string Device(valDevice.GetString(), valDevice.GetStringLength());
		std::string Command(valCommand.GetString(), valCommand.GetStringLength());

		{
			std::stringstream str;
			str << attrDev << ": " << Device;
			SetTo::LocalLog("httpserver", severity_level::trace, str.str());
		}

		{
			std::stringstream str;
			str << attrCmd << ": " << Command;
			SetTo::LocalLog("httpserver", severity_level::trace, str.str());
		}

		{
			std::stringstream str;
			str <<  attrPars << ": ";
			SetTo::LocalLog("httpserver", severity_level::trace, str.str());
		}

		// Stringify parameters
		StringBuffer strParams;
		Writer<StringBuffer> writerParams(strParams);
		valParams.Accept(writerParams);
		SetTo::LocalLog("httpserver", severity_level::trace, strParams.GetString());

		for (Value::ConstMemberIterator itr = valParams.MemberBegin();
			itr != valParams.MemberEnd(); ++itr)
		{
			std::stringstream str;
			str << "Type of parameter '" << itr->name.GetString()
					<< "' is " << kTypeNames[itr->value.GetType()];
			SetTo::LocalLog("httpserver", severity_level::trace, str.str());
		}

		{
			std::stringstream str;
			str << "httpServer::cb_HttpServer: JSON OK!";
			SetTo::LocalLog("httpserver", severity_level::trace, str.str());
		}

		MT::SendEvent(Device, MT::getNameForClient(HttpClient::s_concreteName), Command, strParams.GetString());

	}

}

void httpServerThread()
{
	struct mg_mgr mgr;
	struct mg_connection *nc;

	mg_mgr_init(&mgr, NULL);
	nc = mg_bind(&mgr, "8000", cb_HttpServer);

	// Setup http server parameters
	mg_set_protocol_http_websocket(nc);
	s_http_server_opts.document_root = ".";
	s_http_server_opts.dav_document_root = ".";
	s_http_server_opts.enable_directory_listing = "no";

	// infinite web server cycle
	try
	{
		for(;;)
		{
			boost::this_thread::interruption_point();
			mg_mgr_poll(&mgr, 100);
		}
	}

	catch(...)
	{
		std::stringstream error;
		error << "httpServerThread: Device layer Http server stopped.";
		SetTo::LocalLog("httpserver", severity_level::error, error.str());
	}

	mg_mgr_free(&mgr);
}

boost::thread startHttpServer()
{
	boost::thread thr(httpServerThread);

	return thr;
}

}



#endif /* HTTPSERVER_H_ */
