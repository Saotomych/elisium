// Non Nobis, Domine, non Nobis, sed nomini tuo da gloriam

#ifndef HANDLERS_TARGETHANDLER_H_
#define HANDLERS_TARGETHANDLER_H_

#include <boost/multiprecision/cpp_int.hpp>
#include <handlers/BaseHandler.h>
#include <commons/Logger.h>
#include <commons/PgInterface.h>
#include <commons/HttpClient.h>
#include <commons/AttackAlgo.h>
#include <commons/trim.h>

#include <string>

using namespace boost::multiprecision;

// curl -v -H "Content-Type: application/txt" -X POST -d 'alice target supersystem8' http://127.0.0.1:8000
// curl -v -H "Content-Type: application/txt" -X POST -d 'alice answer ok' http://127.0.0.1:8000
// curl -v -H "Content-Type: application/txt" -X POST -d 'alice answer %--------------------%Node "supersystem8/firewall" properties:%Installed program: #2209900%Type: Firewall%DISABLED for: 483 sec%Child nodes:%0: antivirus1 (Antivirus): #1811628 DISABLED %1: antivirus2 (Antivirus): #16530052 DISABLED %%END ----------------' http://127.0.0.1:8000
// curl -v -H "Content-Type: application/txt" -X POST -d 'alice answer %--------------------%#2209906 programm info:%Effect: fulltrace%Inevitable effect: trace%Allowed node types:% -Firewall% -Antivirus% -VPN% -Brandmauer% -Router% -Traffic monitor% -Cyptographic system%Duration: 900sec.%END ----------------' http://127.0.0.1:8000

class TargetHandler: public BaseHandler
{
	std::string systemName;

public:

	TargetHandler(UserObject& uo): BaseHandler(uo) {}

	virtual bool commandHandler(std::string& pars)
	{
		trim(pars);
		systemName = pars;
		return true;
	}

	// Здесь обрабатывается ответ на устаеовку цели юзером
	virtual bool nextHandler(std::string& pars)
	{
		trim(pars);
		PgInterface& pg = PgInterface::getInstance();

		DBSystem system;
		if (pg.getSystem(systemName, system) == false)
		{
			Logger::CommonLog(error, "DB Error for getSystem");
			return false;
		}

		// Шаг - Добавление информации о типах для добавленного ранее кода защиты
// 		...#9075 programm info:\nEffect: disable\nAllowed node types:\n -Firewall\n -Antivirus\n -VPN\n -Brandmauer\n -Router\n -Traffic monitor\n -Cyptographic system\nDuration: 900sec.\nEND -
		size_t s = pars.find("programm info");
		if ( s != std::string::npos )
		{
			Logger::CommonLog(debug, "Programm Info OK");

			// Parse effects
			std::string effect_o;
			std::string effect_m;
			std::string types;
			uint256_t code;

			if ( ParseInfo(pars, code, effect_o, effect_m, types) == false )
			{
				Logger::CommonLog(error, "DB Error for ParseInfo");
				return false;
			}

			pg.setDefenceTypes(code, types, effect_m, effect_o, uo_.userId());

			return false;
		}

		// Шаг - Добавление кода защиты узла и самого узла
//		...Node "SystemName/firewall" properties:\nInstalled program: #6449300\nType: Firewall\nEND
		s = pars.find("firewall");
		if ( s != std::string::npos )
		{
			Logger::CommonLog(debug, "Firewall OK");

			bool info = false;
			uint256_t defCode = 0;
			s = pars.find('#');
			if (s != std::string::npos)
			{
				std::string num;
				s += 1;
				while (pars[s] >= '0' && pars[s] <= '9')
				{
					num += pars[s];
					s++;
				}

				uint256_t d(num);
				defCode = d;
				std::string typesInFuture;
				std::string effectOFuture;
				std::string effectMFuture;

				pg.setDefence(defCode, typesInFuture, effectMFuture, effectOFuture, uo_.userId());
				info = true;
			}

			std::string fwn("firewall");
			std::string fwt("Firewall");
			pg.setNode(system.id, fwn, fwt, defCode);

			if (info)
			{
				std::stringstream code;
				code << defCode;
				uo_.sendXmpp(std::string("info ") + code.str());
				return true;
			}

			return false;
		}

		// Шаг - Установка узла пользователю, как входного в систему
		s = pars.find("ok");
		if ( s != std::string::npos )
		{
			Logger::CommonLog(debug, "Target OK");
			if (system.name.size() == 0)
			{
				pg.setSystem(systemName);
			}

			std::vector<DBNode> node;
			pg.getNodes(systemName, PgInterface::NodeBy::SYSTEMNAME, node);

			if (node.size())
			{
				pg.setFirewall(uo_.userName(), systemName);
				return false;
			}
			else
			{
				uo_.sendXmpp("look firewall");
				return true;
			}
		}

		return false;
	}
};



#endif /* HANDLERS_TARGETHANDLER_H_ */
