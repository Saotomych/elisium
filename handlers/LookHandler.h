// Non Nobis, Domine, non Nobis, sed nomini tuo da gloriam

#ifndef HANDLERS_LOOKHANDLER_H_
#define HANDLERS_LOOKHANDLER_H_

#include <boost/multiprecision/cpp_int.hpp>
#include <handlers/BaseHandler.h>
#include <commons/Logger.h>
#include <commons/PgInterface.h>
#include <commons/HttpClient.h>
#include <commons/ParseLook.h>
#include <commons/AttackAlgo.h>

#include <string>
#include <sstream>
#include <vector>
#include <cstdlib>

using namespace boost::multiprecision;

// \n--------------------\nNode "ManInBlack/firewall" properties:\nInstalled program: #2209900\nType: Firewall\nDISABLED for: 483 sec\nChild nodes:\n0: antivirus1 (Antivirus): #1811628 DISABLED \n1: antivirus2 (Antivirus): #16530052 DISABLED \n\nEND ----------------


class LookHandler: public BaseHandler
{

	std::string nodeName;

public:

	LookHandler(UserObject& uo): BaseHandler(uo)
	{

	}

	virtual bool commandHandler(std::string& pars)
	{
		trim(pars);
		nodeName = pars;
		return true;
	}

	virtual bool nextHandler(std::string& pars)
	{
		trim(pars);

		std::string nodeName;
		uint256_t defCode;
		std::string Type;
		uint32_t disTime;
		std::vector<std::string> childNames;
		std::vector<std::string> childTypes;
		std::vector<uint256_t> childDefCodes;

		PgInterface& pg = PgInterface::getInstance();

		if ( ParseLook(pars, nodeName, defCode, Type, disTime, childNames, childTypes, childDefCodes) == false )
		{
			std::stringstream code;
			code << defCode;
			Logger::CommonLog(error, "ParseLook failed");
			Logger::CommonLog(debug, "ParseLook not parsed: nodeName: " + nodeName + "; defence code: "
					+ code.str()
					+ "; type = " + Type);
			return false;
		}

		std::stringstream code;
		if (!defCode)
			code << "*encrypted*";
		else
			code << defCode;

		Logger::CommonLog(debug, "ParseLook parsed: nodeName: " + nodeName + "; defence code: "
				+ code.str()
				+ "; type = " + Type);

		// Добавление узлов и ребер
		DBSystem system;
		pg.getUserSystem(uo_.userName(), system);

		std::vector<DBNode> nodes;
		pg.getNodes(system.name, PgInterface::NodeBy::SYSTEMNAME, nodes);

		std::vector<DBEdge> edges;
		pg.getEdges(nodes, edges);

		// look узел
		bool addNode = true;
		for (auto& n: nodes)
		{
			if (n.name == nodeName)
			{
				addNode = false;
				break;
			}
		}
		if (addNode)
		{
			std::string typesInFuture;
			std::string effectOFuture;
			std::string effectMFuture;
			pg.setDefence(defCode, typesInFuture, effectMFuture, effectOFuture, uo_.userId());
			pg.setNode(system.id, nodeName, Type, defCode);
			pg.getNodes(system.name, PgInterface::NodeBy::SYSTEMNAME, nodes);
		}

		uint256_t fromId = 0;
		for (DBNode& n: nodes)
		{
			if (n.name == nodeName)
			{
				fromId = n.id;
				break;
			}
		}
		if (fromId == 0)
			return false;

		if (childNames.size())
		{
			for (uint32_t i = 0; i < childNames.size(); ++i )
			{
				std::stringstream code;
				code << childDefCodes[i];
				std::string child("Name: " + childNames[i] + "; Type: " + childTypes[i] + "; Defence Code: " + code.str() );
				Logger::CommonLog(debug, "ParseLook Child: " + child);

				bool addNode = true;
				for (auto& n: nodes)
				{
					if (n.name == childNames[i])
					{
						addNode = false;
						break;
					}
				}
				if (addNode)
				{
					std::string typesInFuture;
					std::string effectOFuture;
					std::string effectMFuture;
					pg.setDefence(childDefCodes[i], typesInFuture, effectMFuture, effectOFuture, uo_.userId());

					pg.setNode(system.id, childNames[i], childTypes[i], childDefCodes[i]);
					pg.getNodes(system.name, PgInterface::NodeBy::SYSTEMNAME, nodes);
				}

				uint256_t toId = 0;
				for (DBNode& n: nodes)
				{
					if (n.name == childNames[i])
					{
						toId = n.id;
						break;
					}
				}
				if (toId == 0)
					return false;

				bool addEdge = true;
				for (DBEdge e: edges)
				{
					if (e.from == fromId && e.to == toId)
					{
						addEdge = false;
					}
				}

				if (addEdge)
				{
					pg.setEdge(fromId, toId);
					pg.getEdges(nodes, edges);
				}
			}
		}

		if (defCode && pars.find("DISABLED") == std::string::npos)
		{
			AttackAlgo algo;
			uint256_t divider;
			std::vector<DBAttack> atts = algo.Attack(defCode, Type, divider);

			std::stringstream div;
			div << "Divider: " << divider;
			Logger::CommonLog(debug, div.str());
			uo_.sendConsole(div.str());

			std::string attsstr;
			for (DBAttack& v: atts)
			{
				std::stringstream code;
				code << v.code;
				attsstr += "Code: #" + code.str() + ": (" + v.types + ")%";
			}
			if (attsstr.size() == 0)
			{
				std::stringstream att;
				att << "Attack codes not found.";
				uo_.sendConsole(att.str());
			}
			else
			{
				uo_.sendConsole(attsstr);
			}

			Logger::CommonLog(debug, "Attack codes :" + attsstr);
		}
		else
		{
			Logger::CommonLog(debug, "Cryptographic: code unknown");
		}

		return false;
	}

};

#endif /* HANDLERS_LOOKHANDLER_H_ */
