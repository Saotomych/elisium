// Non Nobis, Domine, non Nobis, sed nomini tuo da gloriam

#ifndef ADD_CODE_HANDLER
#define ADD_CODE_HANDLER

#include <boost/multiprecision/cpp_int.hpp>
#include <handlers/BaseHandler.h>
#include <commons/Logger.h>
#include <commons/PgInterface.h>
#include <commons/HttpClient.h>
#include <commons/ParseInfo.h>

#include <string>
#include <sstream>
#include <vector>
#include <cstdlib>

using namespace boost::multiprecision;

// curl -v -H "Content-Type: application/txt" -X POST -d 'alice addcode #180 #310' http://127.0.0.1:8000
// curl -v -H "Content-Type: application/txt" -X POST -d 'alice answer %--------------------%#180 programm info:%Effect: trace%Allowed node types:% -Firewall% -Antivirus% -VPN% -Brandmauer% -Router% -Traffic monitor% -Cyptographic system%Duration: 900sec.%END ----------------' http://127.0.0.1:8000
// curl -v -H "Content-Type: application/txt" -X POST -d 'alice answer %--------------------%#310 programm info:%Effect: fulltrace%Inevitable effect: trace%Allowed node types:% -Firewall% -Antivirus% -VPN% -Brandmauer% -Router% -Traffic monitor% -Cyptographic system%Duration: 900sec.%END ----------------' http://127.0.0.1:8000

class AddCodeHandler: public BaseHandler
{
	std::vector<uint256_t> codes;

	const std::vector<std::string> atackTypes;
	const std::vector<std::string> defenceTypes;

	bool defenceUpdate = false;

public:

	AddCodeHandler(UserObject& uo): BaseHandler(uo),
		atackTypes({ "disable", "get_data", "get_admins", "fin_leak", "scan", "fraud", "lock", "read_data", "spy",
			"sabotage", "download", "neurodump", "hardware hijack", "defunct", "mental_assault", "minor_fraud", "control" }),
		defenceTypes({ "trace", "locate", "disconnect", "logname", "DPI", "deanonymize", "analyze", "fulltrace" })
	{

	}

	virtual bool commandHandler(std::string& pars)
	{
		trim(pars);
		Logger::CommonLog(severity_level::trace, std::string("Start add codes: ") + pars );

		size_t lastpos = 0;
//		while(lastpos != std::string::npos)
		while(lastpos < pars.size())
		{
//			lastpos = pars.find('#', lastpos);
			while(lastpos < pars.size() && pars[lastpos] != '#') lastpos++;
			Logger::CommonLog(severity_level::trace, std::string("Start add code position: ") + std::to_string(lastpos) );

			if (lastpos != std::string::npos)
			{
				std::string num;
				lastpos++;
				while (pars[lastpos] >='0' && pars[lastpos] <= '9')
				{
					num += pars[lastpos];
					lastpos++;
				}
				uint256_t v(num);

				std::cout <<  "Code: " << v << std::endl;

				if (v)
				{
					codes.push_back(v);
					std::stringstream log;
					log << "Added code: " << v;
					Logger::CommonLog(severity_level::debug, log.str() );
				}
			}
		}

		PgInterface& pg = PgInterface::getInstance();

		while( codes.size() )
		{
			std::vector<DBAttack> attacs;
			bool resA = pg.getAttackList(codes[0], std::string(""), attacs);

			std::vector<DBDefence> defs;
			bool resD = pg.getDefenceList(codes[0], std::string(""), defs);

			// Call info
			if (resA == true && attacs.size() == 0 && resD == true && defs.size() == 0)
			{
				std::stringstream code;
				code << "info " << codes[0];
				uo_.sendXmpp(code.str());
				defenceUpdate = false;
				return true;
			}
			else if (resD == true && defs.size() == 1 && defs[0].types.size())
			{
				std::stringstream code;
				code << "info " << codes[0];
				uo_.sendXmpp(code.str());
				defenceUpdate = true;
				return true;
			}

			// Delete first code
			codes.erase(codes.begin());
		}

		return false;
	}

	// Здесь добавляются коды атаки и защиты в базу и вызывается следующий эффект
	virtual bool nextHandler(std::string& pars)
	{
		trim(pars);

		PgInterface& pg = PgInterface::getInstance();

		// Parse effects
		std::string effect_o;
		std::string effect_m;
		std::string types;
		uint256_t code;

		if ( ParseInfo(pars, code, effect_o, effect_m, types) == false)
		{
			Logger::CommonLog(error, "Parse Info failed");
			return false;
		}

		std::stringstream log;
		log << "Answer for code: " << codes[0] << "; Effects & types: m: "
				<< effect_m << "; o: " << effect_o << "; types: " << types;
		Logger::CommonLog(debug, log.str());

		bool att = false;
		bool def = false;
		bool defupdate = false;
		std::cout << "a" << std::endl;
		auto itA = std::find(atackTypes.begin(), atackTypes.end(), effect_o);
		if (itA != atackTypes.end())
		{
			std::cout << "b" << std::endl;
			att = true;
		}
		else
		{
			std::cout << "c" << std::endl;
			auto itD = std::find(defenceTypes.begin(), defenceTypes.end(), effect_o);
			if (itD != defenceTypes.end())
			{
				std::cout << "d" << std::endl;
				def = true;
			}
		}

		std::cout << "1" << std::endl;

		size_t i;
		while ((i=types.find('%'))!=std::string::npos)
		        types.erase(i, 1);

		if (att)
		{
			pg.setAttack(codes[0], types, effect_o, uo_.userId());
			std::cout << "2" << std::endl;
		}
		else if (def)
		{
			if (defenceUpdate)
			{
				pg.setDefenceTypes(codes[0], types, effect_m, effect_o, uo_.userId());
				std::cout << "3" << std::endl;
			}
			else
			{
				pg.setDefence(codes[0], types, effect_m, effect_o, uo_.userId());
				std::cout << "4" << std::endl;
			}
		}
		else
		{
			std::stringstream answ;
			answ << "Answer for code: " << codes[0] << "! Effect is unknown for us: " << effect_o;
			Logger::CommonLog(severity_level::warning, answ.str() );
			uo_.sendConsole(answ.str());
			std::cout << "5" << std::endl;
			return false;
		}

		// Delete first code
		codes.erase(codes.begin());

		while( codes.size() )
		{
			std::vector<DBAttack> attacs;
			bool resA = pg.getAttackList(codes[0], std::string(""), attacs);

			std::vector<DBDefence> defs;
			bool resD = pg.getDefenceList(codes[0], std::string(""), defs);

			// Call info
			if (resA == true && attacs.size() == 0 && resD == true && defs.size() == 0)
			{
				std::cout << "6" << std::endl;
				std::stringstream code;
				code << "info " << codes[0];
				uo_.sendXmpp(code.str());
				defenceUpdate = false;
				return true;
			}
			else if (resD == true && defs.size() == 1 && defs[0].types.size())
			{
				std::cout << "7" << std::endl;
				std::stringstream code;
				code << "info " << codes[0];
				uo_.sendXmpp(code.str());
				defenceUpdate = true;
				return true;
			}

			std::cout << "8" << std::endl;
			// Delete first code
			codes.erase(codes.begin());
		}

		return false;
	}

	virtual ~AddCodeHandler() {}
};

#endif // ADD_CODE_HANDLER

