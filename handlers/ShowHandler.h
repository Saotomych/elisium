/*
 * ShowHandler.h
 *
 *  Created on: Aug 3, 2017
 *      Author: crossenv
 */

#ifndef HANDLERS_SHOWHANDLER_H_
#define HANDLERS_SHOWHANDLER_H_

#include <handlers/BaseHandler.h>
#include <commons/Logger.h>
#include <commons/PgInterface.h>
#include <commons/HttpClient.h>
#include <commons/ParseLook.h>
#include <commons/AttackAlgo.h>
#include <commons/trim.h>

#include <string>
#include <sstream>
#include <vector>
#include <cstdlib>

class ShowHandler: public BaseHandler
{
public:

	ShowHandler(UserObject& uo): BaseHandler(uo)
	{
	}

	virtual bool commandHandler(std::string& pars)
	{
		trim(pars);

		std::string& systemName = pars;
		PgInterface& pg = PgInterface::getInstance();

		std::vector<DBNode> nodes;
		if (pg.getNodes(systemName, PgInterface::NodeBy::SYSTEMNAME, nodes) == false)
		{
			Logger::CommonLog(error, "Node request for system: " + systemName + " failed");
			return false;
		}

		std::vector<DBEdge> edges;
		if (pg.getEdges(nodes, edges) == false)
		{
			Logger::CommonLog(error, "Edge request for system: " + systemName + " failed");
			return false;
		}

		uint32_t num = 0;
		std::string nodeTo("Graph for system " + systemName + "%");
		for (DBNode& n: nodes)
		{
			std::stringstream code;
			code << n.defence_code;
			nodeTo += "* " + std::to_string(num) + " : Name: " + n.name + "; Type: " + n.types + "; Code: #" + code.str() + " -> ";
			uint32_t i=0;
			for (DBEdge& e: edges)
			{
				if ( n.id == e.from )
				{
					for (DBNode& to: nodes)
					{
						if (to.id == e.to)
						{
							DBNode& child = to;
							std::stringstream code;
							code << child.defence_code;
							nodeTo += "Name: " + child.name + "; ";
						}
					}
					++i;
				}
			}

			if (i==0)
			{
				nodeTo += "No nodes";
			}
			else
			{
				nodeTo.erase(nodeTo.end() - 2);
			}
			nodeTo += "%";

			num++;
		}

		uo_.sendConsole(nodeTo);

		return false;
	}

	virtual bool nextHandler(std::string& pars)
	{
		return true;
	}

};

#endif /* HANDLERS_SHOWHANDLER_H_ */
