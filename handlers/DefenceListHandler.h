// Non Nobis, Domine, non Nobis, sed nomini tuo da gloriam

#ifndef HANDLERS_DEFENCELISTHANDLER_H_
#define HANDLERS_DEFENCELISTHANDLER_H_

#include <handlers/BaseHandler.h>
#include <commons/Logger.h>
#include <commons/PgInterface.h>
#include <commons/HttpClient.h>
#include <commons/ParseLook.h>
#include <commons/AttackAlgo.h>
#include <commons/trim.h>

#include <sstream>
#include <string>
#include <sstream>
#include <vector>
#include <cstdlib>


class DefenceListHandler: public BaseHandler
{
public:

	DefenceListHandler(UserObject& uo): BaseHandler(uo)
	{
	}

	virtual bool commandHandler(std::string& pars)
	{
		trim(pars);

		if ( pars.find("#") == std::string::npos)
		{
			std::vector<DBDefence> defs;
			PgInterface& pg = PgInterface::getInstance();

			pg.getDefenceList(0, pars, defs);

			std::string deflist("Defence List: %");
			for (DBDefence& d: defs)
			{
				std::stringstream code;
				code << d.code;
				std::stringstream init;
				init << d.code;
				deflist += "#" + code.str() + "; "
					+ (d.initCode ? init.str() : "") + "; "
					+ (d.types.size() ? ("Types: " + d.types) : "") + "; "
					+ (d.effect_o.size() ? ("Effect: " + d.effect_o) : "") + "; "
					+ (d.effect_m.size() ? ("Inevitable effect: " + d.effect_m) : "")
					+ "%";
			}

			uo_.sendConsole(deflist);
		}
		else
		{
			size_t pos = pars.find("#") + 1;
			std::string num;
			while (pars[pos] >= '0' && pars[pos] <= '9')
			{
				num += pars[pos];
				++pos;
			}

			if (num.size())
			{
				uint256_t defCode(num);

				AttackAlgo algo;
				uint256_t divider;
				std::vector<DBAttack> atts = algo.Attack(defCode, "Cryptographic system", divider);

				std::string attsstr;
				for (DBAttack& v: atts)
				{
					std::stringstream code;
					code << v.code;
					attsstr += "Code: #" + code.str() + ": (" + v.types + ")%";
				}
				if (attsstr.size() == 0)
				{
					std::stringstream att;
					att << "Attack codes not found. Divider: " << divider;
					uo_.sendConsole(att.str());
				}
				else
				{
					uo_.sendConsole(attsstr);
				}

				Logger::CommonLog(debug, "Cryptographic defence code :" + num);
				Logger::CommonLog(debug, "Attack codes :" + attsstr);
			}
			else
			{
				uo_.sendConsole("Defence code incorrect");
			}
		}

		return false;
	}

	virtual bool nextHandler(std::string& pars)
	{
		return false;
	}
};



#endif /* HANDLERS_DEFENCELISTHANDLER_H_ */
