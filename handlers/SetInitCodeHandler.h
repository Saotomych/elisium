/*
 * SetInitCodeHandler.h
 *
 *  Created on: Aug 3, 2017
 *      Author: crossenv
 */

#ifndef HANDLERS_SETINITCODEHANDLER_H_
#define HANDLERS_SETINITCODEHANDLER_H_

#include <boost/multiprecision/cpp_int.hpp>
#include <handlers/BaseHandler.h>
#include <commons/Logger.h>
#include <commons/PgInterface.h>
#include <commons/HttpClient.h>
#include <commons/ParseLook.h>
#include <commons/AttackAlgo.h>
#include <commons/trim.h>

#include <string>
#include <sstream>
#include <vector>
#include <cstdlib>

using namespace boost::multiprecision;

class SetInitCodeHandler: public BaseHandler
{
public:

	SetInitCodeHandler(UserObject& uo): BaseHandler(uo)
	{
	}

	virtual bool commandHandler(std::string& pars)
	{
		PgInterface& pg = PgInterface::getInstance();

		trim(pars);

		uint256_t initCode(pars);

		size_t posbgn = pars.find("#");
		if (posbgn == std::string::npos)
			return false;

		std::string num(&pars.c_str()[posbgn + 1]);
		uint256_t defCode(num);

		pg.setDefenceInitCode(initCode, defCode);

		return false;
	}

	virtual bool nextHandler(std::string& pars)
	{
		return false;
	}
};

#endif /* HANDLERS_SETINITCODEHANDLER_H_ */
