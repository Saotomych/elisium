// Non Nobis, Domine, non Nobis, sed nomini tuo da gloriam
#ifndef HANDLERS_ATTACKLISTHANDLER_H_
#define HANDLERS_ATTACKLISTHANDLER_H_

#include <handlers/BaseHandler.h>
#include <commons/Logger.h>
#include <commons/PgInterface.h>
#include <commons/HttpClient.h>
#include <commons/ParseLook.h>
#include <commons/AttackAlgo.h>
#include <commons/trim.h>

#include <string>
#include <sstream>
#include <vector>
#include <cstdlib>

class AttackListHandler: public BaseHandler
{
public:

	AttackListHandler(UserObject& uo): BaseHandler(uo)
	{
	}

	virtual bool commandHandler(std::string& pars)
	{
		trim(pars);

		std::vector<DBAttack> defs;
		PgInterface& pg = PgInterface::getInstance();

		pg.getAttackList(0, pars, defs);

		std::string deflist("Attack List: %");
		for (DBAttack& d: defs)
		{
			std::stringstream code;
			code << d.code;
			deflist += "#" + code.str() + "; "
				+ (d.types.size() ? ("Types: " + d.types) : "") + "; "
				+ (d.effect.size() ? ("Effect: " + d.effect) : "") + "; "
				+ "%";
		}

		uo_.sendConsole(deflist);

		return false;
	}

	virtual bool nextHandler(std::string& pars)
	{
		return false;
	}
};

#endif /* HANDLERS_ATTACKLISTHANDLER_H_ */
