// Non Nobis, Domine, non Nobis, sed nomine tuo da gloriam
#ifndef HANDLERS_BASEHANDLER_H_
#define HANDLERS_BASEHANDLER_H_

#include <UserObject.h>
#include <string>

class BaseHandler
{
public:

	UserObject& uo_;

	BaseHandler(UserObject& uo): uo_(uo) {}

	virtual bool commandHandler(std::string& pars) = 0;

	virtual bool nextHandler(std::string& pars) = 0;

	virtual ~BaseHandler() {}

};

#endif /* HANDLERS_BASEHANDLER_H_ */
