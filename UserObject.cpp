// Non Nobis, Domine, non Nobis, sed nomini tuo da gloriam

#include <boost/multiprecision/cpp_int.hpp>

#include <UserObject.h>
#include <handlers/AddCodeHandler.h>
#include <handlers/TargetHandler.h>
#include <handlers/LookHandler.h>
#include <handlers/ShowHandler.h>
#include <handlers/DefenceListHandler.h>
#include <handlers/AttackListHandler.h>
#include <handlers/SetInitCodeHandler.h>
#include <chrono>

using namespace boost::multiprecision;

std::map<std::string, UserObject*> UserObject::userHandler;	// ключ это ссылка на юзернейм

void UserObject::UserThread(UserObject* uo)
{
	while(!uo->stopThread)
	{
		std::string cmd;
		std::string pars;
		EventCode ec = uo->waitEvent(cmd, pars);

		if (ec == ANSWER && uo->handler)
		{
			std::stringstream log;
			log << "New Answer for user " << uo->uname_ << "; cmd = " << cmd << "; pars = " << pars;
			Logger::CommonLog( debug, log.str());

			uo->events_.pop();

			if ( uo->handler->nextHandler(pars) == false)
			{
				delete uo->handler;
				uo->handler = nullptr;
				Logger::CommonLog( debug, "Command state machine 'AddCodeHandler' deleted by end state");
				uo->sendConsole("Auto Scenario was finished by success");
			}
		}
		else
		{
			if (ec == ANSWER)
			{
				Logger::CommonLog( debug, "Unexpected ANSWER was received: " + cmd + " " + pars);
				uo->events_.pop();
				if (uo->handler)
				{
					delete uo->handler;
					uo->handler = nullptr;
				}
			}

			if (ec == TOUT)
			{
				if (uo->handler)
				{
					delete uo->handler;
					uo->handler = nullptr;
					Logger::CommonLog( debug, "Command state machine deleted by timeout");
					uo->sendConsole("Auto Scenario was finished by timeout");
				}
			}

			if (ec == ERROR_EVENT)
			{
				std::stringstream log;
				log << "Error Event for user " << uo->uname_ << "; cmd = " << cmd << "; pars = " << pars;
				Logger::CommonLog( debug, log.str());
				uo->events_.pop();
				if (uo->handler)
				{
					delete uo->handler;
					uo->handler = nullptr;
				}
			}

			if (ec == NOT_ANSWER)
			{
				std::stringstream log;
				log << "New Event for user " << uo->uname_ << "; cmd = " << cmd << "; pars = " << pars;
				Logger::CommonLog( debug, log.str());

				uo->events_.pop();

				// Create and start handle for new command
				if (cmd == "addcode")
				{
					uo->handler = new AddCodeHandler(*uo);
				}

				if (cmd == "target")
				{
					uo->handler = new TargetHandler(*uo);
				}

				if (cmd == "look")
				{
					uo->handler = new LookHandler(*uo);
				}

				if (cmd == "show")
				{
					uo->handler = new ShowHandler(*uo);
				}

				if (cmd == "defence")
				{
					uo->handler = new DefenceListHandler(*uo);
				}

				if (cmd == "setinitcode")
				{
					uo->handler = new SetInitCodeHandler(*uo);
				}

				if (cmd == "attack")
				{
					uo->handler = new AttackListHandler(*uo);
				}

				if (uo->handler)
				{
					if (uo->handler->commandHandler(pars) == false)
					{
						delete uo->handler;
						uo->handler = nullptr;
						Logger::CommonLog( debug, "Command state machine 'AddCodeHandler' deleted by end state");
						uo->sendConsole("Auto Scenario was finished by success");
					}
				}
			}
		}
	}
}

UserObject::UserObject(std::string& uname, uint32_t uId): uname_(uname), lastTime(0), uId_(uId) {}

void UserObject::sendXmpp(std::string text)
{
	timeval tv;
	uint256_t tm;
	do
	{
		gettimeofday(&tv, 0);
		tm = tv.tv_sec * 1000000 + tv.tv_usec;
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	} while((lastTime + 1000000) > tm);

	lastTime = tm;

	HttpClient* client = HttpClient::getInstance();
	client->sendHtppMsg(uname_, "mcb", text);
}

void UserObject::sendConsole(std::string text)
{
	HttpClient* client = HttpClient::getInstance();
	client->sendHtppMsg(uname_, "con", text);
}

/// @return enum eventCode
UserObject::EventCode UserObject::waitEvent(std::string& cmd, std::string& pars)
{
	std::unique_lock<std::mutex> lock(mt_);

	if ( cv_.wait_for(lock, std::chrono::milliseconds(10000)) == std::cv_status::timeout)
	{
		return TOUT;
	}

	std::string s = events_.front();

	if ( !s.size() )
	{
		return ERROR_EVENT;
	}

	size_t pos = s.find(' ');
	if (pos == std::string::npos || pos >= s.size() - 2)
	{
		pars = std::string();
		trim(s);
	}
	else
	{
		pars = std::string(s, pos + 1, s.size() - pos );
	}

	s.resize(pos);

	cmd = s;

	if (s == "answer")
		return ANSWER;
	else
		return NOT_ANSWER;
}

std::string& UserObject::userName() { return uname_; }

uint32_t UserObject::userId() { return uId_; }

