//============================================================================
// Name        : PgInterface.cpp
// Author      : alex
// Created on  : 25 May 2016 г.
// Version     : v.0.1
// Copyright   : Non nobis, Domine, non nobis, sed nomini tuo da gloriam.
// Description : Interface to database Postgresql
//============================================================================

// curl -v -H "Content-Type: application/txt" -X POST -d 'alice addcode #18 #31' http://127.0.0.1:8000
// curl -v -H "Content-Type: application/txt" -X POST -d 'alice answer %--------------------%#310 programm info:%Effect: fulltrace%Inevitable effect: trace%Allowed node types:% -Firewall% -Antivirus% -VPN% -Brandmauer% -Router% -Traffic monitor% -Cyptographic system%Duration: 900sec.%END ----------------' http://127.0.0.1:8000

#include <commons/PgInterface.h>
#include <commons/Logger.h>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <sstream>

std::mutex PgInterface::pgmutex;

PgInterface::PgInterface()
{

	Logger::CommonLog(debug, "Connect to PostgreSQL...");

	/*
	 * If the user supplies a parameter on the command line, use it as the
	 * conninfo string; otherwise default to setting dbname=postgres and using
	 * environment variables or defaults for all other connection parameters.
	 */
	conninfo = "dbname=deusex user=dbuser password=EnrjUKfp1";
//	conninfo = "dbname=deusex user=postgres password=postgres";

	/* Make a connection to the database */
	conn = PQconnectdb(conninfo.c_str());

	/* Check to see that the backend connection was successfully made */
	if (PQstatus(conn) != CONNECTION_OK)
	{
		std::string str(PQerrorMessage(conn));
		Logger::CommonLog(error, "ERROR! Connection to database failed: " + str);
	    PQfinish(conn);
		return;
	}

//	PGresult* res;
//	ExecCommandQuery(res, "CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\"");

	/*
	 * Should PQclear PGresult whenever it is no longer needed to avoid memory
	 * leaks
	 */
	Logger::CommonLog(debug, "PostgreSQL connected.");

}

PgInterface::~PgInterface()
{

	Logger::CommonLog(debug, "PostgreSQL disconnect...");

    /* close the connection to the database and cleanup */
    PQfinish(conn);

	Logger::CommonLog(debug, "PostgreSQL disconnected.");
}


bool PgInterface::ExecCommandQuery(PGresult*& res, const char* strQuery)
{

	res = PQexec(conn, strQuery);

    if (PQresultStatus(res) != PGRES_COMMAND_OK)
    {
		std::string str(PQerrorMessage(conn));
		Logger::CommonLog(error, "ERROR! Query '" + std::string(strQuery) + "'  failed: " + str);
        PQclear(res);
        return false;
    }

    PQclear(res);

    return true;
}


bool PgInterface::ExecTuplesQuery(PGresult*& res, const char* strQuery)
{

    res = PQexec(conn, strQuery);

    if (PQresultStatus(res) != PGRES_TUPLES_OK)
    {
		std::string str(PQerrorMessage(conn));
		Logger::CommonLog(error, "ERROR! Query '" + std::string(strQuery) + "'  failed: " + str);
        return false;
    }

    return true;
}


bool PgInterface::ExecTransactionQuery(PGresult*& res, const char* strQuery)
{
	/* Start a transaction block */
	if ( ExecCommandQuery(res, "BEGIN") == false )
		return false;

	if ( ExecCommandQuery(res, strQuery) == false )
		return false;

    /* end the transaction */
	if ( ExecCommandQuery(res, "END") == false )
		return false;

	return true;
}

// --- Public Interface ---

bool PgInterface::getUserList(std::vector<DBUser>& userList)
{
	std::unique_lock<std::mutex> lock1(pgmutex);

	userList.clear();

	// Postgres
	Logger::CommonLog(trace, "PgInterface getUserList called.");

	PGresult* res = nullptr;

	if (ExecCommandQuery(res, "BEGIN") == false)
		return false;

	if (ExecCommandQuery(res, "DECLARE myportal CURSOR FOR SELECT * FROM users") == false)
		return false;

	if (ExecTuplesQuery(res, "FETCH ALL IN myportal") == false)
		return false;

	/* next, print out the rows */
	int32_t ntpl = PQntuples(res);
	int32_t nfld = PQnfields(res);

	{
		std::stringstream str;
		str << "PgInterface getUserList has in result: tuples = " << ntpl << "; fields = " << nfld;
		Logger::CommonLog(trace, str.str());
	}

	for (uint32_t i = 0; i < ntpl; ++i)
	{
		DBUser dbu;
		dbu.userId = atoi( PQgetvalue(res, i, 0) );
		dbu.uname = PQgetvalue(res, i, 1);
		dbu.account = PQgetvalue(res, i, 2);
		dbu.posId = atoi(PQgetvalue(res, i, 4));
		dbu.disabled = std::string(PQgetvalue(res, i, 5)) == "f" ? false : true;
		userList.push_back(dbu);
	}

	PQclear(res);

	if (ExecCommandQuery(res, "END") == false)
		return false;

    /* close the portal ... we don't bother to check for errors ... */
    res = PQexec(conn, "CLOSE");
	PQclear(res);

	for (uint i=0; i < ntpl; ++i)
	{
		Logger::CommonLog(debug, std::string("User ") + std::to_string(userList[i].userId) + ": " + userList[i].uname);
	}

	return true;
}

bool PgInterface::getAttackList(uint256_t filterCode, std::string filterType, std::vector<DBAttack>& attacs)
{
	std::unique_lock<std::mutex> lock1(pgmutex);
	attacs.clear();

	// Postgres
	Logger::CommonLog(trace, "PgInterface getAttackList called.");

	PGresult* res = nullptr;

	if (ExecCommandQuery(res, "BEGIN") == false)
		return false;

	std::stringstream query;

	query << "DECLARE myportal CURSOR FOR SELECT * FROM attacs";
	if (filterCode)
		query << " WHERE attacs.attack_code = '" << filterCode << "'";

	if (ExecCommandQuery(res, query.str().c_str()) == false)
		return false;

	if (ExecTuplesQuery(res, "FETCH ALL IN myportal") == false)
		return false;

	/* next, print out the rows */
	int32_t ntpl = PQntuples(res);
	int32_t nfld = PQnfields(res);

	{
		std::stringstream str;
		str << "PgInterface getUserList has in result: tuples = " << ntpl << "; fields = " << nfld;
		Logger::CommonLog(trace, str.str());
	}

	for (uint32_t i = 0; i < ntpl; ++i)
	{
		DBAttack at;
		std::string atstr(PQgetvalue(res, i, 0));
		uint256_t val(atstr);
		at.code = val;
		at.types = PQgetvalue(res, i, 1);
		at.effect = PQgetvalue(res, i, 2);
		at.compromated = std::string(PQgetvalue(res, i, 3)) == "f" ? false : true;
		at.userId = atoll(PQgetvalue(res, i, 5));

//		struct tm* tm;
//		strptime(PQgetvalue(res, i, 5), "%F %H:%M:%S", tm);
//		at.insertTime = timegm(tm);

		if ( ( filterType.size() && at.types.find(filterType) != std::string::npos ) || (filterType.size() == 0) )
		{
			attacs.push_back(at);
		}
	}

	PQclear(res);

	if (ExecCommandQuery(res, "END") == false)
		return false;

    /* close the portal ... we don't bother to check for errors ... */
    res = PQexec(conn, "CLOSE");
	PQclear(res);

//	for (uint i=0; i < ntpl; ++i)
//	{
//		Logger::CommonLog(debug, std::string("Attack ") + std::to_string(attacs[i].code) + ": " + attacs[i].types);
//	}

	return true;
}

void PgInterface::setAttack(uint256_t code, std::string& types, std::string& effect, uint32_t userId)
{
	std::unique_lock<std::mutex> lock1(pgmutex);
	std::stringstream str;

	str << "INSERT INTO attacs  (attack_code, types, effect, compromated, \"desc\", user_id, time)  VALUES ( '"
			<< code << "', '" << types << "', '" << effect << "', "
			<< "false, ''," << userId
			<< ", CURRENT_TIMESTAMP )";

	PGresult* res = PQexec(conn, str.str().c_str());

	if (PQresultStatus(res) != PGRES_COMMAND_OK)
    {
		std::string str(PQerrorMessage(conn));
		Logger::CommonLog(error, "ERROR! Query '" + str + "'  failed: " + str);
    }

    PQclear(res);
}

bool PgInterface::getDefenceList(uint256_t filterCode, std::string filterType, std::vector<DBDefence>& defences)
{
	std::unique_lock<std::mutex> lock1(pgmutex);
	defences.clear();

	// Postgres
	Logger::CommonLog(trace, "PgInterface getDefenceList called.");

	PGresult* res = nullptr;

	if (ExecCommandQuery(res, "BEGIN") == false)
		return false;

	std::stringstream query;
	query << "DECLARE myportal CURSOR FOR SELECT * FROM defences";
	if (filterCode)
		query << " WHERE defences.defence_code = '" << filterCode << "'";

	if (ExecCommandQuery(res, query.str().c_str()) == false)
		return false;

	if (ExecTuplesQuery(res, "FETCH ALL IN myportal") == false)
		return false;

	/* next, print out the rows */
	int32_t ntpl = PQntuples(res);
	int32_t nfld = PQnfields(res);

	{
		std::stringstream str;
		str << "PgInterface getUserList has in result: tuples = " << ntpl << "; fields = " << nfld;
		Logger::CommonLog(trace, str.str());
	}

	for (uint32_t i = 0; i < ntpl; ++i)
	{
		DBDefence dfc;

		std::string str1(PQgetvalue(res, i, 0));
		uint256_t val1(str1);
		dfc.code = val1;

		std::string str2(PQgetvalue(res, i, 1));
		uint256_t val2(str2);
		dfc.initCode = val2;

		dfc.types = PQgetvalue(res, i, 2);
		dfc.effect_m = PQgetvalue(res, i, 3);
		dfc.effect_o = PQgetvalue(res, i, 4);
		dfc.compromated = std::string(PQgetvalue(res, i, 5)) == "f" ? false : true;
		dfc.userId = atoll(PQgetvalue(res, i, 6));

//		struct tm* tm;
//		strptime(PQgetvalue(res, i, 5), "%F %H:%M:%S", tm);
//		at.insertTime = timegm(tm);

		if ( ( filterType.size() && dfc.types.find(filterType) != std::string::npos ) || (filterType.size() == 0) )
		{
			defences.push_back(dfc);
		}
	}

	PQclear(res);

	if (ExecCommandQuery(res, "END") == false)
		return false;

    /* close the portal ... we don't bother to check for errors ... */
    res = PQexec(conn, "CLOSE");
	PQclear(res);

//	for (uint i=0; i < ntpl; ++i)
//	{
//		Logger::CommonLog(debug, std::string("Defence ") + std::to_string(defences[i].code) + ": " + defences[i].types);
//	}
	
	return true;
}

void PgInterface::setDefence(uint256_t code, std::string& types, std::string& effect_m, std::string& effect_o, uint32_t userId)
{
	std::unique_lock<std::mutex> lock1(pgmutex);

	Logger::CommonLog(trace, "PgInterface setDefence called.");

	std::stringstream str;

	str << "INSERT INTO defences  (defence_code, init_code, types, effect_m, effect_o, compromated, \"desc\", user_id, time)  VALUES ( '"
			<< code << "', 0, '" << types << "', '" << effect_m << "', '" << effect_o << "', "
			<< "false, ''," << userId
			<< ", CURRENT_TIMESTAMP )";

	PGresult* res = PQexec(conn, str.str().c_str());

	if (PQresultStatus(res) != PGRES_COMMAND_OK)
    {
		std::string str(PQerrorMessage(conn));
		Logger::CommonLog(error, "ERROR! Query '" + str + "'  failed: " + str);
    }

    PQclear(res);
}

void PgInterface::setDefenceTypes(uint256_t code, std::string& types, std::string& effect_m, std::string& effect_o, uint32_t userId)
{
	std::unique_lock<std::mutex> lock1(pgmutex);

	Logger::CommonLog(trace, "PgInterface setDefenceTypes called");
	Logger::CommonLog(trace, "PgInterface setDefenceTypes: effect_m = " + effect_m);
	Logger::CommonLog(trace, "PgInterface setDefenceTypes: effect_o = " + effect_o);
	Logger::CommonLog(trace, "PgInterface setDefenceTypes: types = " + types);

	std::stringstream str;
	str << "UPDATE defences SET types = '" << types  << "', "
		<< "user_id = " << userId << ", "
		<< "effect_o = '" << effect_o << "', "
		<< "effect_m = '" << effect_m << "' "
		<< " WHERE defence_code = '" << code << "'";

	PGresult* res = PQexec(conn, str.str().c_str());

	if (PQresultStatus(res) != PGRES_COMMAND_OK)
    {
		std::string str(PQerrorMessage(conn));
		Logger::CommonLog(error, "ERROR! Query '" + str + "'  failed: " + str);
    }

    PQclear(res);
}

// Записывает init_code в defences
void PgInterface::setDefenceInitCode (uint256_t init_code, uint256_t defence_code)
{
	std::unique_lock<std::mutex> lock1(pgmutex);

	Logger::CommonLog(trace, "PgInterface setDefenceInitCode called.");

	std::stringstream str;
	str << "UPDATE defences SET init_code = '" << init_code << "' WHERE defence_code = '" << defence_code << "'";
	
	PGresult* res = PQexec(conn, str.str().c_str());
	
	if (PQresultStatus(res) != PGRES_COMMAND_OK)
    {
		std::string str(PQerrorMessage(conn));
		Logger::CommonLog(error, "ERROR! Query '" + str + "'  failed: " + str);
    }

    PQclear(res);
	
}

//Получаем систему по имени пользователя
bool PgInterface::getUserSystem (std::string uname, DBSystem& system)
{
	std::unique_lock<std::mutex> lock1(pgmutex);

	Logger::CommonLog(trace, "PgInterface getUserSystem called.");
	
	PGresult* res = nullptr;
	
	if (ExecCommandQuery(res, "BEGIN") == false)
		return false;
	
	std::string query("DECLARE myportal CURSOR FOR SELECT systems.id, systems.name, systems.desc"
	" FROM users JOIN nodes ON users.position = nodes.id"
	" JOIN systems ON nodes.system_id = systems.id"
	" WHERE users.username = '" + uname + "'");
	
	if (ExecCommandQuery(res, query.c_str()) == false)
		return false;

	if (ExecTuplesQuery(res, "FETCH ALL IN myportal") == false)
		return false;

	/* next, print out the rows */
	int32_t ntpl = PQntuples(res);
	int32_t nfld = PQnfields(res);

	{
		std::stringstream str;
		str << "PgInterface getUserSystem has in result: tuples = " << ntpl << "; fields = " << nfld;
		Logger::CommonLog(trace, str.str());
	}
	
	if (ntpl)
	{
		system.id = atoll( PQgetvalue(res, 0, 0) );
		system.name = PQgetvalue(res, 0, 1);
		system.desc = PQgetvalue(res, 0, 2);
	}
	
	PQclear(res);

	if (ExecCommandQuery(res, "END") == false)
		return false;

    /* close the portal ... we don't bother to check for errors ... */
    res = PQexec(conn, "CLOSE");
	PQclear(res);

	if (ntpl)
		Logger::CommonLog(debug, std::string("System ") + system.name + "; desc: " + system.desc);
	
	return true;
	
}

//Записывает ID файервола в таблицу пользователя
bool PgInterface::setFirewall(std::string& uname, std::string& systemName)
{
	std::unique_lock<std::mutex> lock1(pgmutex);

	Logger::CommonLog(trace, "PgInterface setFirewall called.");
	
	PGresult* res = nullptr;
	
	if (ExecCommandQuery(res, "BEGIN") == false)
		return false;
	
	std::string query("DECLARE myportal CURSOR FOR SELECT nodes.id FROM nodes, systems "
	"WHERE nodes.system_id = systems.id AND systems.name = '" + systemName + "' AND nodes.name = 'firewall'");
	
	if (ExecCommandQuery(res, query.c_str()) == false)
		return false;

	if (ExecTuplesQuery(res, "FETCH ALL IN myportal") == false)
		return false;

	/* next, print out the rows */
	int32_t ntpl = PQntuples(res);
	int32_t nfld = PQnfields(res);

	{
		std::stringstream str;
		str << "PgInterface getUserSystem has in result: tuples = " << ntpl << "; fields = " << nfld;
		Logger::CommonLog(trace, str.str());
	}
	
	uint256_t firewall_id;
	
	for (uint32_t i = 0; i < ntpl; ++i)
	{
		firewall_id = atoll( PQgetvalue(res, i, 0) );
	}
	
	PQclear(res);
	
	if (ExecCommandQuery(res, "END") == false)
		return false;

    /* close the portal ... we don't bother to check for errors ... */
    res = PQexec(conn, "CLOSE");
	PQclear(res);

	// update
	//////////////////////////////////////////////////////////////////////////
	
	if (ExecCommandQuery(res, "BEGIN") == false)
		return false;

	std::stringstream str;
	str << "UPDATE users SET position = " << firewall_id << " WHERE username = '" << uname << "'";
	
	res = PQexec(conn, str.str().c_str());
	
	if (PQresultStatus(res) != PGRES_COMMAND_OK)
    {
		std::string str(PQerrorMessage(conn));
		Logger::CommonLog(error, "ERROR! Query '" + str + "'  failed: " + str);
    }

    PQclear(res);

	if (ExecCommandQuery(res, "END") == false)
		return false;

    /* close the portal ... we don't bother to check for errors ... */
    res = PQexec(conn, "CLOSE");
	PQclear(res);
	////////////////////////////////////////////////////////////////////
	return true;
}

bool PgInterface::getNodes(std::string uname, NodeBy selector, std::vector<DBNode>& nodes)
{
	std::unique_lock<std::mutex> lock1(pgmutex);

	nodes.clear();

	// Postgres
	Logger::CommonLog(trace, "PgInterface getNodes called.");

	PGresult* res = nullptr;

	if (ExecCommandQuery(res, "BEGIN") == false)
		return false;

	std::string query;

	//если известно имя пользователя

	if (selector == NodeBy::USERNAME)
	{
		query = std::string("DECLARE myportal CURSOR FOR SELECT * FROM nodes AS nds"
		"WHERE nds.system_id = (SELECT nodes.system_id FROM users, nodes WHERE users.position = nodes.id"
		"AND users.username = '" + uname + "')");
	}
		

	//если известен id системы
	if (selector == NodeBy::SYSTEMID)
	{
		query = std::string("DECLARE myportal CURSOR FOR SELECT * FROM nodes WHERE system_id = '" + uname + "'");
	}

	
	//если известно имя системы
	if (selector == NodeBy::SYSTEMNAME)
	{
		query = std::string("DECLARE myportal CURSOR FOR SELECT * FROM nodes WHERE system_id = (SELECT id FROM systems WHERE name = '" + uname + "')");
	}
	
	if (ExecCommandQuery(res, query.c_str()) == false)
		return false;

	if (ExecTuplesQuery(res, "FETCH ALL IN myportal") == false)
		return false;

	/* next, print out the rows */
	int32_t ntpl = PQntuples(res);
	int32_t nfld = PQnfields(res);

	{
		std::stringstream str;
		str << "PgInterface getNodes has in result: tuples = " << ntpl << "; fields = " << nfld;
		Logger::CommonLog(trace, str.str());
	}

	for (uint32_t i = 0; i < ntpl; ++i)
	{
		DBNode dbn;
		dbn.id = atoll( PQgetvalue(res, i, 0) );
		dbn.systemId = atoll( PQgetvalue(res, i, 2) );
		dbn.name = PQgetvalue(res, i, 3);
		dbn.types = PQgetvalue(res, i, 4);

		std::string str1(PQgetvalue(res, i, 5));
		uint256_t val1(str1);
		dbn.defence_code = val1;

		std::string str2(PQgetvalue(res, i, 6));
		uint256_t val2(str2);
		dbn.attack_code = val2;

		dbn.encrypted = std::string(PQgetvalue(res, i, 7)) == "f" ? false : true;
		
		nodes.push_back(dbn);
	}

	PQclear(res);

	if (ExecCommandQuery(res, "END") == false)
		return false;

    /* close the portal ... we don't bother to check for errors ... */
    res = PQexec(conn, "CLOSE");
	PQclear(res);

	for (uint i=0; i < ntpl; ++i)
	{
		Logger::CommonLog(debug, std::string("Nodes ") + nodes[i].name + ": " + nodes[i].types);
	}
	return true;
}

bool PgInterface::getEdges(std::vector<DBNode>& nodes, std::vector<DBEdge>& edges)
{
	// Postgres
	std::unique_lock<std::mutex> lock1(pgmutex);

	Logger::CommonLog(trace, "PgInterface getEdges called.");

	std::string nodeIds;
	
	for(uint i=0; i < nodes.size(); ++i) {
		nodeIds += std::to_string(nodes[i].id);
		if(i != nodes.size() - 1) {
			nodeIds += ",";
		}
	}

	if (nodeIds.size() == 0)
		return true;
	
	edges.clear();
	
	PGresult* res = nullptr;

	if (ExecCommandQuery(res, "BEGIN") == false)
		return false;

	std::string query("DECLARE myportal CURSOR FOR SELECT * FROM edges WHERE edges.from in (" + nodeIds + ")");
	
		if (ExecCommandQuery(res, query.c_str()) == false)
		return false;

	if (ExecTuplesQuery(res, "FETCH ALL IN myportal") == false)
		return false;

	/* next, print out the rows */
	int32_t ntpl = PQntuples(res);
	int32_t nfld = PQnfields(res);

	{
		std::stringstream str;
		str << "PgInterface getNodes has in result: tuples = " << ntpl << "; fields = " << nfld;
		Logger::CommonLog(trace, str.str());
	}
	
	for (uint32_t i = 0; i < ntpl; ++i)
	{
		DBEdge dbe;
		dbe.id = atoll( PQgetvalue(res, i, 0) );
		dbe.from = atoll( PQgetvalue(res, i, 1) );
		dbe.to = atoll( PQgetvalue(res, i, 2) );
		
		edges.push_back(dbe);
	}
	
	PQclear(res);

	if (ExecCommandQuery(res, "END") == false)
		return false;

    /* close the portal ... we don't bother to check for errors ... */
    res = PQexec(conn, "CLOSE");
	PQclear(res);

	for (uint i=0; i < ntpl; ++i)
	{
		Logger::CommonLog(debug, std::string("Edges ") + std::to_string(edges[i].id));
	}
	return true;
}

bool PgInterface::getSystem(std::string& systemName, DBSystem& system)
{
	// Postgres
	std::unique_lock<std::mutex> lock1(pgmutex);

	Logger::CommonLog(trace, "PgInterface getSystem called.");

	PGresult* res = nullptr;

	if (ExecCommandQuery(res, "BEGIN") == false)
		return false;

	std::string query("DECLARE myportal CURSOR FOR SELECT * FROM systems WHERE systems.name = '" + systemName + "'");

	if (ExecCommandQuery(res, query.c_str()) == false)
		return false;

	if (ExecTuplesQuery(res, "FETCH ALL IN myportal") == false)
		return false;

	/* next, print out the rows */
	int32_t ntpl = PQntuples(res);
	int32_t nfld = PQnfields(res);

	{
		std::stringstream str;
		str << "PgInterface getSystem has in result: tuples = " << ntpl << "; fields = " << nfld;
		Logger::CommonLog(trace, str.str());
	}

	if (ntpl)
	{
		system.id = atoll( PQgetvalue(res, 0, 0) );
		system.name = PQgetvalue(res, 0, 1);
		system.desc = PQgetvalue(res, 0, 2);
	}

	PQclear(res);

	if (ExecCommandQuery(res, "END") == false)
		return false;

    /* close the portal ... we don't bother to check for errors ... */
    res = PQexec(conn, "CLOSE");
	PQclear(res);

	if (ntpl)
	{
		Logger::CommonLog(debug, std::string("System id:") + std::to_string(system.id) + "; name: " + system.name + "; desc: " + system.desc);
	}

	return true;
}

bool PgInterface::setSystem(std::string& systemName)
{
	std::unique_lock<std::mutex> lock1(pgmutex);

	Logger::CommonLog(trace, "PgInterface setSystem called.");

	std::stringstream str;

	str << "INSERT INTO systems (name, \"desc\")  VALUES ( '" << systemName << "', '')";

	PGresult* res = PQexec(conn, str.str().c_str());

	if (PQresultStatus(res) != PGRES_COMMAND_OK)
    {
		std::string str(PQerrorMessage(conn));
		Logger::CommonLog(error, "ERROR! Query '" + str + "'  failed: " + str);
		return false;
    }

    PQclear(res);
    return true;
}

// Если defCode = 0, то узел encrypted
bool PgInterface::setNode(uint32_t systemId, std::string& name, std::string& type, uint256_t defCode)
{
	std::unique_lock<std::mutex> lock1(pgmutex);

	Logger::CommonLog(trace, "PgInterface setNode called.");

	std::stringstream str;

	str << "INSERT INTO nodes (cluster_id,system_id,name,type,defence_code,attack_code,encrypted,\"desc\",info) "
		"VALUES ( 0, " << systemId << ", '" << name << "', '" << type << "', '"
		<< defCode << "', null, " << (defCode ? "false" : "true") << ", '', '')";

	PGresult* res = PQexec(conn, str.str().c_str());

	if (PQresultStatus(res) != PGRES_COMMAND_OK)
    {
		std::string str(PQerrorMessage(conn));
		Logger::CommonLog(error, "ERROR! Query '" + str + "'  failed: " + str);
		return false;
    }

    PQclear(res);
    return true;
}

bool PgInterface::setEdge(uint256_t from, uint256_t to)
{
	std::unique_lock<std::mutex> lock1(pgmutex);

	Logger::CommonLog(trace, "PgInterface setNode called.");

	std::stringstream str;

	str << "INSERT INTO edges (\"from\",\"to\") "
		"VALUES ( '" << from << "', '" << to << "')";

	PGresult* res = PQexec(conn, str.str().c_str());

	if (PQresultStatus(res) != PGRES_COMMAND_OK)
    {
		std::string str(PQerrorMessage(conn));
		Logger::CommonLog(error, "ERROR! Query '" + str + "'  failed: " + str);
		return false;
    }

    PQclear(res);
    return true;
}
