#ifndef DEBUG_LOG
#define DEBUG_LOG

// TODO: delete DEBUG define and set DEBUG in Makefile for Debug config
#define DEBUG	// temporary

#include <iostream>
#include <fstream>
#include <string>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

class ConsoleLog;

namespace StreamStdStrings
{
	template <typename T>
	void toStream(const T& p, std::ostream* fLog, boost::mutex& tmut, boost::posix_time::ptime& bgntime, bool& ts)
	{
		boost::mutex::scoped_lock lock(tmut);

		if (ts)
		{
			ts = false;

			boost::posix_time::ptime t(boost::posix_time::microsec_clock::local_time());
			if (fLog)
				*fLog << (t-bgntime) << ": ";
			else
				std::cerr << (t-bgntime) << ": ";
		}
		if (fLog)
			*fLog << p;
		else
			std::cerr << p;
	}
}

class ConsoleLog
{
	bool ts;
	boost::posix_time::ptime bgntime;
	boost::mutex mut;
	boost::mutex tmut;

#ifdef LOGTOFILE
	std::filebuf fBuf;
	std::ostream* fLog;
#endif
	
	ConsoleLog(): ts(true), bgntime(boost::posix_time::microsec_clock::local_time())
	{
#ifdef LOGTOFILE
		fBuf.open("project.log", std::ios::out);
		fLog = new std::ostream(&fBuf);
#endif
	}

public:

	static ConsoleLog& Log()
	{
		static ConsoleLog *l=NULL;
		static boost::mutex mut;

		boost::mutex::scoped_lock lock(mut);

		if (!l) l = new ConsoleLog();
		l->locklog();
		return *l;
	}

	ConsoleLog& locklog() {

#ifdef DEBUG
		mut.lock();
#endif
		return *this; 
	}

	typedef std::basic_ostream<char, std::char_traits<char> > CoutType;
	typedef CoutType& (*StandardEndLine)(CoutType&);
	
#ifdef DEBUG
	template <typename T>
	ConsoleLog& operator <<(const T& p)
	{
#ifdef LOGTOFILE
		StreamStdStrings::toStream(p, fLog, tmut, bgntime, ts);
#else
		StreamStdStrings::toStream(p, nullptr, tmut, bgntime, ts);
#endif // LOGTOFILE
		return *this;
	}


	ConsoleLog& operator <<(StandardEndLine e)
    {
		boost::mutex::scoped_lock lock(tmut);

#ifdef LOGTOFILE
		*fLog << std::endl;
		fLog->flush();
#else
		std::cerr << std::endl;
		std::cerr.flush();
#endif // LOGTOFILE

		ts = true;
		mut.unlock();

		return *this;
    }
#else

	template <typename T>
	ConsoleLog& operator <<(const T& p)
	{
		return *this;
	}

	ConsoleLog& operator <<(StandardEndLine e)
    {
		return *this;
    }
#endif
	
};

#endif // DEBUG_LOG

