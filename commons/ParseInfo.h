// Non Nobis, Domine, non Nobis, sed nomini tuo da gloriam

#ifndef COMMONS_PARSEINFO_H_
#define COMMONS_PARSEINFO_H_

#include <boost/multiprecision/cpp_int.hpp>

#include <string>
#include <stdlib.h>

using namespace boost::multiprecision;

bool ParseInfo(std::string& pars, uint256_t& code, std::string& effect_o, std::string& effect_m, std::string& types)
{
	// Parse effects
	std::string estr("Effect: ");
	std::string istr("Inevitable effect: ");
	std::string ant("Allowed node types:%");

	size_t posbgn = pars.find(estr);
	if (posbgn == std::string::npos)
	{
		std::cout << "info: a" << std::endl;
		return false;
	}

	size_t posend = pars.find("%", posbgn);
	if (posend == std::string::npos)
	{
		std::cout << "info: b" << std::endl;
		return false;
	}

	std::string s1(pars, posbgn + estr.size(), posend - posbgn - estr.size());
	effect_o = s1;

	posbgn = pars.find(istr);
	if (posbgn != std::string::npos)
	{
		posend = pars.find("%", posbgn);
		if (posend == std::string::npos)
		{
			std::cout << "info: c" << std::endl;
			return false;
		}

		std::string s2(pars, posbgn + istr.size(), posend - posbgn - istr.size());
		effect_m = s2;
	}

	posbgn = pars.find(ant);
	if (posbgn == std::string::npos)
	{
		std::cout << "info: d" << std::endl;
		return false;
	}

	posend = pars.find("END", posbgn + ant.size());
	if (posend == std::string::npos)
	{
		std::cout << "info: e" << std::endl;
		return false;
	}

	std::string s3(pars, posbgn + ant.size(), posend - posbgn - ant.size());
	types = s3;

	std::string afc("#");
	posbgn = pars.find(afc);
	if (posbgn == std::string::npos)
	{
		std::cout << "info: f" << std::endl;
		return false;
	}

	std::string num;
	posbgn += afc.size();
	while (pars[posbgn] >= '0' && pars[posbgn] <= '9')
	{
		num += pars[posbgn];
		posbgn++;
	}

	uint256_t c(num);
	code = c;

	return true;
}



#endif /* COMMONS_PARSEINFO_H_ */
