/*
 * Graph.h
 *
 *  Created on: Jul 31, 2017
 *      Author: crossenv
 */
// Non Nobis, Domine, non Nobis, sed nomini tuo da gloriam

#ifndef COMMONS_GRAPH_H_
#define COMMONS_GRAPH_H_

// Реализация графа системы

#include <commons/PgInterface.h>

#include <vector>
#include <queue>

class Graph
{
public:

	Graph() {}

	void addNode(DBNode& node)
	{
		for (auto& n: nodes)
		{
			if (n.name == node.name)
				return;
		}

		nodes.push_back(node);

		for (auto& v1: graph)
		{
			v1.push_back(0);
		}

		std::vector<uint32_t> v(graph[0].size(), 0);
		graph.push_back(v);
	}

	void addEdge(std::string from, std::string to)
	{
		int32_t iFrom = -1;
		for (auto& n: nodes)
		{
			iFrom++;
			if (n.name == from)
			{
				break;
			}
		}
		if (iFrom == -1) return;

		DBNode* iTo = -1;
		for (auto& n: nodes)
		{
			iTo++;
			if (n.name == from)
			{
				break;
			}
		}
		if (iTo == -1) return;

		graph[iFrom][iTo] = 1;
	}

	bool initIterator()
	{
		if (!graph.size())
			return false;

		visit.resize( graph[0].size(), 0 );
		widthGo.push(iterx);
		return true;
	}

	bool next(const DBNode& node)
	{
		if (widthGo.size())
		{
			iterx = widthGo.front();
			visit[iterx] = 1;
			widthGo.pop();
		}
		else
		{
			return false;
		}

		uint32_t child = 0;
		while (child != -1)
		{
			child = findUnvisitedChild(iterx);
			if (child != -1 && visit[child] == 0)
				widthGo.push(child);
		}

		return true;
	}

private:

	uint32_t findUnvisitedChild(uint32_t x)
	{
		uint32_t size = nodes.size();
		for (uint32_t y = 0; y < size; ++y )
		{
			if ( graph[x][y] == 1 && visit[y] == 0 )
			{
				return y;
			}
		}

		return -1;
	}

	uint32_t findNextParent(uint32_t y, uint32_t startX)
	{
		uint32_t size = nodes.size();
		for (uint32_t x = startX; x < size; ++x )
		{
			if ( graph[x][y] == 1 )
			{
				return x;
			}
		}

		return -1;
	}

	std::vector<DBNode> nodes;
	std::vector<uint8_t> visit;
	std::vector< std::vector<uint32_t> > graph;
	uint32_t iterx, itery;

	std::queue<uint32_t> widthGo;
};

#endif /* COMMONS_GRAPH_H_ */
