/*
 * ParseLook.h
 *
 *  Created on: Aug 2, 2017
 *      Author: crossenv
 */

#ifndef COMMONS_PARSELOOK_H_
#define COMMONS_PARSELOOK_H_

#include <boost/multiprecision/cpp_int.hpp>

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <cstdlib>

using namespace boost::multiprecision;

// %--------------------%Node "ManInBlack/firewall" properties:%Installed program: #2209900%Type: Firewall%DISABLED for: 483 sec%
// Child nodes:%0: antivirus1 (Antivirus): #1811628 DISABLED %1: antivirus2 (Antivirus): #16530052 DISABLED %%END ----------------

bool ParseLook(
		std::string& pars,
		std::string& nodeName,
		uint256_t& defCode,
		std::string& Type,
		uint32_t& disTime,
		std::vector<std::string>& childNames,
		std::vector<std::string>& childTypes,
		std::vector<uint256_t>& childDefCodes
		)
{
	std::string nodestr("/");
	std::string propsstr("properties");
	std::string defCodestr("Installed program: #");
	std::string defCodestrEnc("Installed program: ");
	std::string typestr("Type: ");
	std::string disabledT("DISABLED for: ");

	// Name
	size_t posbgn = pars.find(nodestr);
	if (posbgn == std::string::npos)
		return false;

	size_t posend = pars.find(propsstr, posbgn);
	if (posend == std::string::npos)
		return false;

	std::string s1(pars, posbgn+1, posend-posbgn-3);

	std::cout << s1 <<std::endl;
	std::cout << (posbgn+1) <<std::endl;
	std::cout << (posend-posbgn-3) <<std::endl;

	nodeName = s1;

	/// defCode
	posbgn = pars.find(defCodestr);
	if (posbgn == std::string::npos)
	{
		posbgn = pars.find(defCodestrEnc);
		if (posbgn == std::string::npos)
		{
			std::cout << "2a" << std::endl;
			return false;
		}
	}

	posend = pars.find("%", posbgn);
	if (posend == std::string::npos)
	{
		std::cout << "2b" << std::endl;
		return false;
	}

	std::string s2(pars, posbgn + defCodestr.size(), posend-posbgn-defCodestr.size());

	std::cout << s2 <<std::endl;
	std::cout << (posbgn + defCodestr.size()) <<std::endl;
	std::cout << (posend-posbgn-defCodestr.size()) <<std::endl;

	if (s2 == "encrypted*")
		defCode = 0;
	else
	{
		uint256_t d(s2);
		defCode = d;
	}

	/// Type
	posbgn = pars.find(typestr);
	if (posbgn == std::string::npos)
	{
		std::cout << "3a" << std::endl;
		return false;
	}

	posend = pars.find("%", posbgn);
	if (posend == std::string::npos)
	{
		std::cout << "3b" << std::endl;
		return false;
	}

	std::string s3(pars, posbgn + typestr.size(), posend - posbgn - typestr.size());
	Type = s3;

	/// DISABLE TIME
	disTime = 0;
	posbgn = pars.find(disabledT);
	if (posbgn != std::string::npos)
	{
		posend = pars.find("%", posbgn);
		if (posend == std::string::npos)
		{
			std::cout << "4b" << std::endl;
			return false;
		}

		std::string s4(pars, posbgn + disabledT.size(), posend - posbgn - disabledT.size());
		disTime = atoi(s4.c_str());
	}

	// TODO: Остальные данные существуют при взломанном узле, сейчас не парсятся но сделать надо
	// Child nodes:%0: antivirus1 (Antivirus): #1811628 DISABLED %1: antivirus2 (Antivirus): #16530052 DISABLED %%END ----------------

	std::string childstr("Child nodes:");
	posbgn = pars.find(childstr);

	uint32_t numcheck = 0;
	if (posbgn != std::string::npos)
	{
		posbgn += childstr.size();
		uint num = 0;
		while(posbgn != std::string::npos)
		{
			posbgn++;
			uint32_t num = atoi(&pars.c_str()[posbgn]);

			std::cout << "child parse cycle " << numcheck << " vs " << num << std::endl;

			if (num != numcheck)
				break;

			// parse childs
			posbgn += 3;
			posend = pars.find(" ", posbgn);
			if (posend == std::string::npos)
			{
				std::cout << "5a: num = " << num << std::endl;
				return false;
			}

			std::string sname(pars, posbgn, posend-posbgn);
			childNames.push_back(sname);

			posbgn = pars.find("(", posbgn);
			if (posbgn == std::string::npos)
			{
				std::cout << "6a: num = " << num << std::endl;
				return false;
			}

			posend = pars.find(")", posbgn);
			if (posend == std::string::npos)
			{
				std::cout << "7a: num = " << num << std::endl;
				return false;
			}

			std::string stype(pars, posbgn+1, posend - posbgn - 1);
			childTypes.push_back(stype);

			posbgn = pars.find("#", posbgn);
			if (posbgn == std::string::npos)
			{
				std::cout << "8a: num = " << num << std::endl;
				return false;
			}

			std::string nm;
			posbgn += 1;
			while (pars[posbgn] >= '0' && pars[posbgn] <= '9')
			{
				nm += pars[posbgn];
				posbgn++;
			}
			uint256_t defcode(nm);
			childDefCodes.push_back(defcode);

			std::cout << "child parse result: " << sname << "; " << stype << "; " << defcode << std::endl;

			numcheck++;

			posbgn = pars.find("%", posbgn);
		}
	}

	return true;
}

#endif /* COMMONS_PARSELOOK_H_ */
