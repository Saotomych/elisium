// Copyright   : Non nobis, Domine, non nobis, sed nomini tuo da gloriam.

#ifndef HTTPSERVER_H_
#define HTTPSERVER_H_

#include <UserObject.h>

#include <mongoose/mongoose.h>
#include <commons/Logger.h>

#include <thread>
#include <memory>

/*
 * Test message for server:
 * curl -v -H "Content-Type: application/json" -X POST
 * -d '{"device":"barrier_in", "command":"down",
 * "parameters":{"state":"open", "car":"present"} }'
 * http://localhost:8000
 */

// curl -v -H "Content-Type: application/json" -X POST -d '{"device":"barrier_in", "command":"down", "parameters":{"state":"open", "car":"present"} }' http://localhost:8000

namespace httpserver
{

struct mg_serve_http_opts s_http_server_opts;

bool stop = false;

void cb_HttpServer(struct mg_connection* nc, int ev, void* p)
{
	struct http_message *hmsg = (struct http_message*) p;

	if (ev == MG_EV_HTTP_REQUEST)
	{
		Logger::CommonLog(severity_level::trace, "httpServer::cb_HttpServer: HTTP Server received new data.");

//		HOWTO: take remote address and port in mongoose
//		char* addr = inet_ntoa(nc->sa.sin.sin_addr); // client address
//		int port = nc->sa.sin.sin_port;

		nc->flags |= MG_F_SEND_AND_CLOSE;
		mg_printf(nc, "%s", "HTTP/1.1 200 OK\r\n\r\n\r\n");

		// Check body lenght
		if (hmsg->body.len == 0)
		{
			// отправить назад сообщение, что body не существует
			std::stringstream error;
			error << "ERROR! httpServer::cb_HttpServer: POST body size is NULL.";
			Logger::CommonLog(severity_level::error, error.str());

			return;
		}

	    std::string msg(hmsg->body.p, hmsg->body.len);
		Logger::CommonLog(severity_level::debug, std::string("httpServer::cb_HttpServer: received message: ") + msg );

		size_t pos = msg.find(' ');
		if (pos == std::string::npos)
			return;

		if ((hmsg->body.len - pos) < 2)
			return;

		std::string userName(msg, 0, pos);
		std::string toHandle(msg, pos+1, hmsg->body.len - pos);

		Logger::CommonLog(severity_level::debug, std::string("httpServer::cb_HttpServer: message for user ") + userName + ": " + toHandle );

		auto uoIt = UserObject::userHandler.find(userName);
		if (uoIt != UserObject::userHandler.end())
		{
			UserObject* uo = uoIt->second;

			uo->mt_.lock();
			uo->events_.push(toHandle);
			uo->mt_.unlock();
			uo->cv_.notify_one();
		}
	}
}

void httpServerThread()
{
	struct mg_mgr mgr;
	struct mg_connection *nc;

	mg_mgr_init(&mgr, NULL);
	nc = mg_bind(&mgr, "8000", cb_HttpServer);

	// Setup http server parameters
	mg_set_protocol_http_websocket(nc);
	s_http_server_opts.document_root = ".";
	s_http_server_opts.dav_document_root = ".";
	s_http_server_opts.enable_directory_listing = "no";

	// infinite web server cycle
	try
	{
		while(!stop)
		{
			mg_mgr_poll(&mgr, 100);
		}
	}
	catch(...)
	{
		std::stringstream error;
		error << "httpServerThread: Device layer Http server stopped.";
		Logger::CommonLog(severity_level::error, error.str());
	}

	Logger::CommonLog(info, "HttpServer stopped");

	mg_mgr_free(&mgr);
}

void startHttpServer()
{
	std::thread thr(httpServerThread);
	thr.detach();

	return;
}

}

#endif /* HTTPSERVER_H_ */
