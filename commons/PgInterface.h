//============================================================================
// Name        : PgInterface.h
// Author      : alex
// Created on  : 25 May 2016 г.
// Version     : v.0.1
// Copyright   : Non nobis, Domine, non nobis, sed nomini tuo da gloriam.
// Description : Interface to database Postgresql
//============================================================================

#ifndef PGINTERFACE_H_
#define PGINTERFACE_H_

#include <boost/multiprecision/cpp_int.hpp>
#include <type_traits>
#include <cstdlib>
#include <string>
#include <vector>
#include <time.h>
#include <mutex>

#include <commons/Logger.h>
#include <postgresql/libpq-fe.h>

using namespace boost::multiprecision;

struct DBUser
{
	uint32_t userId;
	std::string uname;
	std::string account;
	uint256_t posId;
	bool disabled;
};

struct DBAttack
{
	uint256_t code;
	std::string types;
	std::string effect;
	bool compromated;
	uint32_t userId;	// кто добавил
	time_t insertTime;  // когда добавил
};

struct DBDefence
{
	uint256_t code;
	uint256_t initCode;	// код инициализации, может быть null, то есть uint256_t.max()
	std::string types;
	std::string effect_m;
	std::string effect_o;
	bool compromated;
	uint32_t userId;	// кто добавил
	time_t insertTime;  // когда добавил
};

struct DBNode
{
	uint32_t id; //чтобы собрать эджи
	uint256_t systemId;
	std::string name;
	std::string types;
	uint256_t defence_code;
	uint256_t attack_code;
	bool encrypted;

};

struct DBSystem
{
	uint32_t id;
	std::string name;
	std::string desc;
	
};

struct DBEdge
{
	uint32_t id;
	uint256_t from;
	uint256_t to;
	
};

class PgInterface
{

private:

	PgInterface();

	static std::mutex pgmutex;
	std::string conninfo;
	PGconn     *conn;

	bool ExecCommandQuery(PGresult*& res, const char* strQuery);
	bool ExecTuplesQuery(PGresult*& res, const char* strQuery);
	bool ExecTransactionQuery(PGresult*& res, const char* strQuery);
public:

	enum class NodeBy { USERNAME = 1, SYSTEMID, SYSTEMNAME };

	static PgInterface& getInstance()
	{
		static PgInterface pg;
		return pg;
	}

	~PgInterface();

	bool getUserList(std::vector<DBUser>& userList);

	void getUserByUserName(std::string& uname, DBUser& user);

	bool getAttackList(uint256_t filterCode, std::string filterType, std::vector<DBAttack>& attacs);

	void setAttack(uint256_t code, std::string& types, std::string& effect, uint32_t userId);

	bool getDefenceList(uint256_t filterCode, std::string filterType, std::vector<DBDefence>& defences);

	void setDefence(uint256_t code, std::string& types, std::string& effect_m, std::string& effect_o, uint32_t userId);

	void setDefenceTypes(uint256_t code, std::string& types, std::string& effect_m, std::string& effect_o, uint32_t userId);

	void getNodeListForTarget(std::vector<DBNode>& nodes);
	
	void setDefenceInitCode (uint256_t init_code, uint256_t defence_code);
	
	bool getUserSystem (std::string userName, DBSystem& system);
	
	bool setFirewall(std::string& uname, std::string& systemName);
	
	bool getNodes(std::string uname, NodeBy selector, std::vector<DBNode>& nodes);
	
	bool getEdges(std::vector<DBNode>& nodes, std::vector<DBEdge>& edges);

	bool getSystem(std::string& systemName, DBSystem& system);

	bool setSystem(std::string& systemName);

	bool setNode(uint32_t systemId, std::string& name, std::string& type, uint256_t defCode);

	bool setEdge(uint256_t from, uint256_t to);
};

#endif /* PGINTERFACE_H_ */
