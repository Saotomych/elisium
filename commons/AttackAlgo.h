// Non Nobis, Domine, non Nobis, sed nomini tuo da gloriam

#ifndef COMMONS_ATTACKALGO_H_
#define COMMONS_ATTACKALGO_H_

#include <boost/multiprecision/cpp_int.hpp>
#include <commons/PgInterface.h>

#include <vector>
#include <cmath>

using namespace boost::multiprecision;

class AttackAlgo
{

public:

	static std::vector<uint256_t> simples;

	static void fillsimples()
	{
		simples.push_back(2);
		simples.push_back(3);
		simples.push_back(5);
		for (uint256_t i = 3; i < 100001; i+=2)
		{
			uint32_t j = 3;
			for (j; j < (i/2); ++j)
			{
				if (i%j == 0)
					break;
			}
			if (j == (i/2))
			{
				simples.push_back(i);
			}
		}
	}

	static uint256_t mindiv(uint256_t a)
	{
		for (uint32_t i = 0; i < simples.size(); ++i)
		{
			if ((a % simples[i]) == 0)
			{
				return simples[i];
			}
		}

		uint256_t sqrres = sqrt(a);
		for (uint256_t i = 100001; i < sqrres; ++i)
		{
			if (a % i == 0)
			{
				return i;
			}
		}

		return 1;
	}

	std::vector<DBAttack> Attack(uint256_t defence, std::string type, uint256_t& divider)
	{
		PgInterface& pg = PgInterface::getInstance();

		uint256_t etadiv = mindiv(defence);
		divider = etadiv;

		std::vector<DBAttack> attacks;
		pg.getAttackList(0, type, attacks);
		std::vector<DBAttack> filtered_attacks;

		for (DBAttack& a: attacks)
		{
			if (( a.code % etadiv ) == 0)
			{
				std::cout << "Found attack: " << a.code << std::endl;
				filtered_attacks.push_back(a);
			}

			if (filtered_attacks.size() >= 10)
			{
				break;
			}
		}

		return filtered_attacks;
	}
};

#endif /* COMMONS_ATTACKALGO_H_ */
