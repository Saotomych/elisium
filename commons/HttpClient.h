// Copyright   : Non nobis, Domine, non nobis, sed nomini tuo da gloriam.

#ifndef HTTPCLIENT_H_
#define HTTPCLIENT_H_

#include <mongoose/mongoose.h>
#include <commons/Logger.h>

#include <mutex>
#include <stdlib.h>

#define FRONTEND_HTTP "http://127.0.0.1:80/in.php"

class HttpClient
{
	struct mg_mgr mgr;

	HttpClient()
	{
		mg_mgr_init(&mgr, NULL);
	}

public:

	static HttpClient* getInstance()
	{
		static HttpClient hc;
		return &hc;
	}

	~HttpClient()
	{
		mg_mgr_free(&mgr);
	}

	std::vector<uint8_t> rcvData;

	static volatile int s_exit_flag;

	static std::mutex HTTPconnectMutex;

	static void ev_handler(struct mg_connection *nc, int ev, void *ev_data) {
	  struct http_message *hm = (struct http_message *) ev_data;

		switch (ev) {

		case MG_EV_CONNECT:
			if (* (int *) ev_data != 0) {
				{
					std::stringstream log;
					log << "HttpClient::ev_handler: Connect to businness logic failed: " << strerror(* (int *) ev_data);
					Logger::CommonLog(error, log.str());
				}

				s_exit_flag = 1;
			}
			break;

		case MG_EV_HTTP_REPLY:
			{
				nc->flags |= MG_F_CLOSE_IMMEDIATELY;
				{
					std::stringstream log;
					log << "HttpClient::ev_handler: message transferred, response code: " << hm->resp_code;
					Logger::CommonLog(trace, log.str());
				}
				s_exit_flag = 1;

			}
			break;

		default:
			break;

	  }
	}

	void sendHtppMsg(const std::string& userName, const std::string type, const std::string& command)
	{

		std::unique_lock<std::mutex> lock(HTTPconnectMutex);

		{
			std::stringstream log;
			log << "sendHtppMsg: " << userName << "; command: " << command;
			Logger::CommonLog(trace, log.str());
		}

		std::string curl("curl -v -H \"Content-Type: application/x-www-form-urlencoded\" -X POST -d '" + userName + " " + type + " " + command + "' http://127.0.0.1:80/in.php");
		if ( system(curl.c_str()) == -1 )
		{
			Logger::CommonLog(error, "Command shell error with curl");
			return;
		}

		{
			std::stringstream log;
			log << "sendHtppMsg: command sent from " << userName << "; command: " << command;
			Logger::CommonLog(debug, log.str());
		}
	}

};

#endif /* HTTPCLIENT_H_ */
