
#include <boost/multiprecision/cpp_int.hpp>
#include <commons/Logger.h>
#include <commons/HttpServer.h>
#include <commons/HttpClient.h>
#include <commons/PgInterface.h>
#include <UserObject.h>
#include <commons/AttackAlgo.h>

#include <signal.h>

#include <utility>
#include <chrono>
#include <thread>
#include <mutex>

volatile int HttpClient::s_exit_flag;
std::mutex HttpClient::HTTPconnectMutex;

std::vector<uint256_t> AttackAlgo::simples;

int main(int argc, char* argv[])
{
	// Инит логгера
	Logger::initDebugLogging();
	Logger::CommonLog(severity_level::info, "Elisium started..");

	Logger::CommonLog(severity_level::info, "Start calculate simple array..");
	AttackAlgo::fillsimples();
	Logger::CommonLog(severity_level::info, "End calculate simple array..");

	// Настройка перехвата сигналов
	sigset_t sigset;
	int32_t sig;

	sigemptyset(&sigset);
	sigaddset(&sigset, SIGINT);
	sigaddset(&sigset, SIGQUIT);
	sigaddset(&sigset, SIGTERM);
	sigaddset(&sigset, SIGTSTP);

	// Подключение к БД
	PgInterface& pg = PgInterface::getInstance();

	// Взять список юзеров
	std::vector<DBUser> userList;
	pg.getUserList(userList);

	// Создать мап юзер-объектов
	// Запустить треды с параметром юзер-объект по числу юзеров
	for (DBUser& user: userList)
	{
		UserObject::userHandler.emplace( std::make_pair( user.uname, new UserObject(user.uname, user.userId) ) );
		UserObject::userHandler[user.uname]->thr_.reset(new std::thread(UserObject::UserThread, UserObject::userHandler[user.uname]));
	}

	// Запустить веб-сервер
	httpserver::startHttpServer();

	// Цикл обработки сигналов
	for(;;)
	{

		sigprocmask(SIG_BLOCK, &sigset, NULL);
		sigwait(&sigset, &sig);

		switch(sig)
		{
		case SIGINT:
		case SIGQUIT:
		case SIGTERM:
		case SIGTSTP:
			Logger::CommonLog(info, "Main: Catched signal to stopping application");
			httpserver::stop = true;

			std::this_thread::sleep_for(std::chrono::milliseconds(200));

			return 0;

		default:
			{
				std::stringstream log;
				log << "ERROR! Main: Catched not registered signal " << sig;
				Logger::CommonLog(error, log.str());
			}
			break;
		}
	}


}
